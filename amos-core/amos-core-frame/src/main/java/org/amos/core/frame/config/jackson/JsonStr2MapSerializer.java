package org.amos.core.frame.config.jackson;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.amos.core.basic.utils.JsonUtils;

import java.io.IOException;
import java.util.Map;

/**
 * string 序列化时转 map
 */
public class JsonStr2MapSerializer extends JsonSerializer<String> {
    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        // 将0序列化为false，将1序列化为true
        if (StrUtil.isNotBlank(value)) {
            jsonGenerator.writePOJO(JsonUtils.jsonToPojo(value, Map.class));
        } else {
            jsonGenerator.writePOJO(null);
        }
    }
}
