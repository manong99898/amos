package org.amos.core.frame.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * int 序列化时转 boolean
 */
public class JsonInt2BoolSerializer extends JsonSerializer<Integer> {
    // 自定义序列化逻辑
    @Override
    public void serialize(Integer value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        // 将0序列化为false，将1序列化为true
        if (Integer.valueOf(1).equals(value)) {
            jsonGenerator.writeBoolean(true);
        } else {
            jsonGenerator.writeBoolean(false);
        }
    }
}
