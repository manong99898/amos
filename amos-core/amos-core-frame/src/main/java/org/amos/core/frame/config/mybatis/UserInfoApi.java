package org.amos.core.frame.config.mybatis;

/**
 * 用户信息接口
 */
public interface UserInfoApi {
    String getCurrentUserName();
}
