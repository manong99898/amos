package org.amos.core.frame.utils;

import cn.hutool.core.lang.Snowflake;

/**
 * @desc: id 生成器 工具类
 * @author: liubt
 * @date: 2022-08-25 17:20
 **/
public class IdUtils {
    /**
     * 雪花算法生成唯一id
     *
     * @return
     */
    public static Long nextId() {
        // 风险点 部署超31台机器会出现id冲突情况
        return new Snowflake().nextId();
    }
}
