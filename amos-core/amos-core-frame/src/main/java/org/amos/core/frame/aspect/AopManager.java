package org.amos.core.frame.aspect;

import org.amos.core.basic.annotation.Log;

import java.lang.reflect.Method;

/**
 * @desc: 切面自定义处理接口
 * @author: liubt
 * @date: 2022-08-11 11:05
 **/
public interface AopManager {
    /**
     * 自定义切面处理方法
     * @param log
     * @param clazz
     * @param method
     * @param args
     * @param resultInfo 返回结果信息
     * @param level
     * @param time
     * @throws Exception
     */
    void doCustom(Log log, Class<?> clazz, Method method, Object[] args, String resultInfo, String level, Long time) throws Exception;
}
