package org.amos.core.basic.annotation;

import org.amos.core.basic.enums.SqlKeyWordEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * mybatis-plus 查询注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MpQuery {
    /**
     * 条件的关键字
     */
    SqlKeyWordEnum keyword() default SqlKeyWordEnum.EQ;

    /**
     * 对应数据库数据列
     */
    String column() default "";
}
