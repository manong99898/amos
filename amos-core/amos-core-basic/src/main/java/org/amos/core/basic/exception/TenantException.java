package org.amos.core.basic.exception;

import lombok.Getter;

@Getter
public class TenantException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TenantException(String message) {
        super(message);
    }

    public TenantException(Throwable cause) {
        super(cause);
    }

    public TenantException(String message, Throwable cause) {
        super(message, cause);
    }
}
