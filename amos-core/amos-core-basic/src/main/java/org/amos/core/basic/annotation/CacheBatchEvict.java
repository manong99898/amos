package org.amos.core.basic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author admin
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheBatchEvict {
    /**
     * 缓存名数组
     */
    String[] cacheNames() default {};

    /**
     * 缓存key值,通过spEL表达式获取参数
     *
     * @return
     */
    String key() default "";
}
