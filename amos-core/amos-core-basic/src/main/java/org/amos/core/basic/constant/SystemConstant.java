package org.amos.core.basic.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统常量
 */
public interface SystemConstant {

    /**
     * 根包名
     */
    String SYS_PACKAGE = "org.amos";
    /******************************************* 系统参数 *******************************************/
    /**
     * 系统默认角色、用户等系统级ID,不可删除
     */
    Long SYS_DEFAULT_ID = 1L;
    /**
     * 租户管理员角色别名
     */
    String STS_TENANT_ADMIN_ROLE_ALIAS = "tenant_admin";
    /**
     * 租户管理员角色别名
     */
    String STS_TENANT_ADMIN_ROLE_NAME = "管理员";
    /**
     * 租户管理员昵称
     */
    String STS_TENANT_ADMIN_USER_NAME = "管理员";
    /**
     * 有效
     */
    Integer SYS_EFFECTIVE_STATUS = 1;
    /**
     * 无效
     */
    Integer SYS_INVALID_STATUS = 0;
    /**
     * 默认删除标记
     */
    Integer SYS_DELETE_FLAG_DEFAULT = 0;
    /**
     * 已经删除标记
     */
    Integer SYS_DELETE_FLAG_ALREADY = 1;

    /**
     * 默认删除字段
     */
    String SYS_DELETE_FLAG_DEFAULT_NAME = "is_deleted";

    /**
     * 非管理员标记
     */
    Integer SYS_NOT_ADMIN = 0;
    /**
     * 管理员标记
     */
    Integer SYS_IS_ADMIN = 1;

    /**
     * 默认分页参数，当前页
     */
    Integer SYS_PAGINATION_PAGE = 1;
    /**
     * 默认分页参数，每页显示数据量
     */
    Integer SYS_PAGINATION_SIZE = 10;
    /**
     * 租户编号字段
     */
    String SYS_TENANT_NO_NAME = "tenant-no";
    /**
     * 父级ID字段
     */
    String SYS_PARENT_ID_NAME = "parent_id";
    /**
     * 默认排序字段
     */
    String SYS_DEFAULT_ORDER_NAME = "create_time";
    /**
     * 默认排序规则
     */
    String SYS_DEFAULT_ORDER_BY = "desc";
    /**
     * 树节点根id
     */
    Long SYS_TREE_ROOT_ID = 0L;
    /**
     * 创建人字段
     */
    String CREATE_USER = "createBy";
    /**
     * 创建时间子弹
     */
    String CREATE_TIME = "createTime";
    /**
     * 修改人字段
     */
    String UPDATE_USER = "updateBy";
    /**
     * 修改时间字段
     */
    String UPDATE_TIME = "updateTime";

    /**
     * traceId
     */
    String TRACE_ID = "traceId";

    /**
     * 全部菜单类型
     */
    Integer MENU_TYPE_ALL = -1;

    /**
     * 权限按钮菜单类型
     */
    Integer MENU_TYPE_PERMISSION = 0;
    /**
     * 默认菜单类型
     */
    Integer MENU_TYPE_NORMAL = 1;
    /**
     * 本地存储文件URI
     */
    String SYS_LOCAL_FILE_PATH = "/file-storage/public/";

    @Getter
    @AllArgsConstructor
    enum WhetherEnums {
        YES(1, true),
        NOT(0, false);

        final Integer type;
        final Boolean status;

        public static Boolean getStatus(Integer type) {
            for (WhetherEnums whether : WhetherEnums.values()) {
                if (whether.type.equals(type)) {
                    return whether.status;
                }
            }
            return null;
        }
    }

    @Getter
    @AllArgsConstructor
    enum MenuTypeEnums {
        menu,iframe,link,button
    }

    /******************************************* 系统用户状态 *******************************************/
    /**
     * 用户默认状态，未激活
     */
    Integer SYS_SYSTEM_USER_STATUS_DEFAULT = 0;
    /**
     * 用户默认状态，已激活
     */
    Integer SYS_SYSTEM_USER_STATUS_ALREADY_ACTIVE = 1;
    /**
     * 用户默认状态，已锁定
     */
    Integer SYS_SYSTEM_USER_STATUS_ALREADY_LOCK = 2;
    /**
     * 用户默认状态，已禁用
     */
    Integer SYS_SYSTEM_USER_STATUS_ALREADY_FORBIDDEN = 3;

    /**
     * 系统用户默认密码
     */
    String SYS_DEFAULT_PWD = "123456";
    /**
     * 二级认证默认过期时间120(秒)
     */
    Integer SUB_SAFE_DEFAULT_EXPIRE = 120;

    /******************************************* Quartz 任务状态 *******************************************/
    /**
     * 正常状态
     */
    Integer QUARTZ_JOB_STATUS_NORMAL = 1;
    /**
     * 停止状态
     */
    Integer QUARTZ_JOB_STATUS_DISABLE = 0;
    /**
     * 参数名称
     */
    String QUARTZ_JOB_PARAMS_NAME = "parameter";
}
