package org.amos.core.basic.base;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import java.io.Serializable;

@Data
public class BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 当前页
     */
    private Integer current = SystemConstant.SYS_PAGINATION_PAGE;
    /**
     * 每页显示数据量
     */
    private Integer size = SystemConstant.SYS_PAGINATION_SIZE;
    /**
     * 排序字段
     */
    @MpQuery(keyword = SqlKeyWordEnum.ORDER_NAME)
    private String orderName = SystemConstant.SYS_DEFAULT_ORDER_NAME;
    /**
     * 排序规则
     */
    @MpQuery(keyword = SqlKeyWordEnum.ORDER_BY)
    private String order = SystemConstant.SYS_DEFAULT_ORDER_BY;

}
