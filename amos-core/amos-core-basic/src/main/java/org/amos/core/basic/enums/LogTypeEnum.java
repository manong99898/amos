package org.amos.core.basic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LogTypeEnum {
    DEFAULT(0, "操作日志"),
    LOGIN(1, "登录日志"),
    SCHEDULED(2, "定时任务"),
    THREAD(3, "后台进程");

    final int type;
    final String name;
}
