package org.amos.core.basic.utils.crud;

import lombok.Data;

import java.io.Serializable;

/**
 * @desc: sql between 条件封装
 * @author: liubt
 * @date: 2022-08-08 13:21
 **/
@Data
public class BetweenCondition<T> implements Serializable {
    private T start;
    private T end;
}
