package org.amos.core.basic.constant;

public interface CacheConstant {
    /**
     * 系统缓存Key
     */
    String SYSTEM_KEY = "amos:sys";
    /**
     * 验证码缓存Key
     */
    String CAPTCHA_KEY = SYSTEM_KEY + ":captcha:";
    /**
     * 手机验证码缓存Key
     */
    String MOBILE_CAPTCHA_KEY = SYSTEM_KEY + ":captcha:mobile:";
}
