package org.amos.core.basic.annotation;

import org.amos.core.basic.enums.LogTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @desc: 日志注解
 * @author: liubt
 * @date: 2022-08-06 17:06
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {
    /**
     * 日志类型
     */
    LogTypeEnum type() default LogTypeEnum.DEFAULT;
    /**
     * 日志名称
     */
    String value() default "";
    /**
     * 是否记录方法返回信息
     */
    boolean logResultInfo() default true;
}
