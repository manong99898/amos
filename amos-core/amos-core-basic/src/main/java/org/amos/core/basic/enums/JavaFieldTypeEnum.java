package org.amos.core.basic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public enum JavaFieldTypeEnum {
    INTEGER(0, "Integer"),
    LONG(1, "Long"),
    FLOAT(2, "Float"),
    DOUBLE(3,  "Double"),
    BIG_DECIMAL(4, "BigDecimal"),
    BOOLEAN(5, "Boolean"),
    STRING(6, "String"),
    DATE(7, "Date");

    final int type;
    final String name;

    public static List<JavaType> dictList() {
        List<JavaType> list = new ArrayList<>();
        for (JavaFieldTypeEnum type : JavaFieldTypeEnum.values()) {
            JavaType javaType = new JavaType();
            javaType.setTypeName(type.name);
            javaType.setTypeValue(type.type);
            list.add(javaType);
        }
        return list;
    }
}
