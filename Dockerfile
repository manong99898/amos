# 拉取最小化的jre 1.8的运行环境（anapsix/alpine-java 项目名称，8_server-jre_unlimited为标签tag）
FROM anapsix/alpine-java:8_server-jre_unlimited
#设置语言包为中文
ENV LANG C.UTF-8
#指定容器时间
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
#创建文件夹
RUN mkdir -p /root/apps
#切换到该文件夹下
WORKDIR /root/apps
#暴露容器端口
EXPOSE 8088
#复制打包文件路径到容器路径
ADD amos.jar ./app.jar
#配置容器，使其可执行化，相当于执行命令
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar"]
#CMD指定启动容器时执行的命令，只能有一条CMD指令，如果指定了多条指令，则最后一条执行
CMD ["--spring.profiles.active=dev"]
