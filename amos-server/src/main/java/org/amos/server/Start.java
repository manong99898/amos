package org.amos.server;

import org.amos.core.basic.constant.SystemConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author lbt
 */
@SpringBootApplication(scanBasePackages = {SystemConstant.SYS_PACKAGE + ".*"})
@EnableCaching
public class Start {
    public static void main(String[] args) {
        SpringApplication.run(Start.class, args);
    }
}
