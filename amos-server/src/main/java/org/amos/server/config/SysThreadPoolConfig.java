package org.amos.server.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.amos.core.frame.config.thread.AbstractAsyncExecutor;
import org.amos.server.properties.DefaultThreadPoolPropertiesProperties;
import org.amos.server.properties.MQThreadPoolPropertiesProperties;
import org.amos.server.properties.PushThreadPoolPropertiesProperties;
import org.amos.server.properties.SysThreadPoolPropertiesProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class SysThreadPoolConfig extends AbstractAsyncExecutor {
    private final DefaultThreadPoolPropertiesProperties defaultThreadPoolProperties;
    private final MQThreadPoolPropertiesProperties mqThreadPoolProperties;
    private final PushThreadPoolPropertiesProperties pushThreadPoolPropertiesProperties;
    private final SysThreadPoolPropertiesProperties sysThreadPoolPropertiesProperties;

    /**
     * 默认线程池
     */
    @Bean(name = "sysExecutor")
    public Executor sysAsyncExecutor() {
        return init(sysThreadPoolPropertiesProperties, (r, executor) -> {
            log.info("队列已满.......");
        });
    }

    /**
     * 默认线程池
     */
    @Bean(name = "defaultExecutor")
    public Executor defaultAsyncExecutor() {
        return init(defaultThreadPoolProperties, new ThreadPoolExecutor.AbortPolicy());
    }

    /**
     * 日志线程池
     */
    @Bean(name = "logAsyncExecutor")
    public Executor logAsyncExecutor() {
        return init(defaultThreadPoolProperties, new ThreadPoolExecutor.AbortPolicy());
    }

    @Bean(name = "mqExecutor")
    public Executor mqAsyncExecutor() {
        return init(mqThreadPoolProperties, new ThreadPoolExecutor.CallerRunsPolicy());
    }

    @Bean(name = "pushExecutor")
    public Executor pushAsyncExecutor() {
        return init(pushThreadPoolPropertiesProperties, new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
