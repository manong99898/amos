/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.entity.Role;
import org.amos.server.modules.upms.entity.UserRole;

import java.util.Set;

/**
 * <p>
 * 用户角色关联 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IUserRoleService extends IService<UserRole> {
    /**
     * 根据userId获取角色集合
     *
     * @param userId
     * @return
     */
    Set<Role> findRolesByUserId(Long userId);
    /**
     * 根据userId获取角色ID集合
     *
     * @param userId
     * @return
     */
    Set<Long> findRoleIdsByUserId(Long userId);

    /**
     * 更新用户权限
     *
     * @param userId
     * @param roleIds
     * @return
     */
    Boolean updateUserRole(Long userId, Set<Long> roleIds);

    /**
     * 批量删除用户权限
     *
     * @param userIds
     * @return
     */
    Boolean removeByUserIds(Set<Long> userIds);
}
