package org.amos.server.modules.upms.service;

import org.amos.server.modules.upms.entity.Log;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ILogLoginService extends IService<Log> {

}