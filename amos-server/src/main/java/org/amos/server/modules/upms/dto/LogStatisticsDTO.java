package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.enums.SqlKeyWordEnum;

/**
 * 系统日志
 */
@Data
public class LogStatisticsDTO {
    @ApiModelProperty(value = "操作类")
    private String clazz;
    @ApiModelProperty(value = "返回结果信息")
    private String resultInfo;
    @ApiModelProperty(value = "ip")
    private String ip;
    @ApiModelProperty(value = "日志级别")
    @MpQuery(column = "level", keyword = SqlKeyWordEnum.EQ)
    private String level;
    @ApiModelProperty(value = "操作方法")
    private String method;
    @ApiModelProperty(value = "日志名称")
    private String name;
    @ApiModelProperty(value = "请求参数")
    private String params;
    @ApiModelProperty(value = "链路ID")
    private String traceId;
    @ApiModelProperty(value = "耗时")
    private Long time;

    @ApiModelProperty(value = "时间区间条件")
    @MpQuery(column = "create_time", keyword = SqlKeyWordEnum.BETWEEN)
    private String timeCondition;

}