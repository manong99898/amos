package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单列表DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class MenuListDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    @MpQuery(column = "id", keyword = SqlKeyWordEnum.IN)
    private List<Long> ids;

    @MpQuery(column = "title", keyword = SqlKeyWordEnum.LIKE)
    private String title;

    @MpQuery(column = "name", keyword = SqlKeyWordEnum.LIKE)
    private String name;

    @MpQuery(column = "type", keyword = SqlKeyWordEnum.EQ)
    private Integer type;

    @MpQuery(column = "path", keyword = SqlKeyWordEnum.LIKE)
    private String path;

    @MpQuery(column = "component", keyword = SqlKeyWordEnum.LIKE)
    private String component;

    @MpQuery(column = "remark", keyword = SqlKeyWordEnum.LIKE)
    private String remark;
}
