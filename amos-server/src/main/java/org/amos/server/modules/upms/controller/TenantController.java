/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.utils.JsonUtils;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.TenantDTO;
import org.amos.server.modules.upms.dto.TenantPackageDTO;
import org.amos.server.modules.upms.entity.Tenant;
import org.amos.server.modules.upms.entity.TenantPackage;
import org.amos.server.modules.upms.service.ITenantPackageService;
import org.amos.server.modules.upms.service.ITenantService;
import org.amos.server.modules.upms.vo.TenantListVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 租户表 前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2021-01-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/tenant")
public class TenantController extends BaseController {

    private final ITenantService tenantService;
    private final ITenantPackageService tenantPackageService;

    @GetMapping("/index-list")
    @ApiOperation(value = "租户列表信息(简单列表，下拉选择用)")
    public R indexList(TenantDTO dto) {
        QueryWrapper<Tenant> qw = buildWrapper(dto);
        qw.select(AmosUtils.toDbField(Tenant::getNo), AmosUtils.toDbField(Tenant::getName));
        List<Tenant> list = tenantService.list(qw);
        List<TenantListVO> voList = AmosUtils.listCopy(list, TenantListVO.class);
        return R.ok(voList);
    }
    @GetMapping("/list")
    @ApiOperation(value = "租户列表信息")
    @Log(value = "租户列表信息", logResultInfo = false)
    @SaCheckPermission("tenant:list")
    public R list(TenantDTO dto) {
        List<Tenant> list = tenantService.list(buildWrapper(dto));
        return R.ok(list);
    }

    @GetMapping("/package/list")
    @ApiOperation(value = "租户套餐列表信息")
    @Log(value = "租户套餐列表信息", logResultInfo = false)
    @SaCheckPermission("tenant:list")
    public R packageList(TenantPackageDTO dto) {
        List<TenantPackage> list = tenantPackageService.list(buildWrapper(dto));
        return R.ok(list);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新租户数据")
    @Log(value = "更新租户数据", logResultInfo = false)
    @SaCheckPermission("tenant:update")
    @SaCheckSafe
    public R update(@RequestBody @Validated TenantDTO dto) {
        tenantService.saveOrUpdateTenant(dto);
        return R.ok();
    }

    @PostMapping("/package/update")
    @ApiOperation(value = "更新租户套餐数据")
    @Log(value = "更新租户套餐数据", logResultInfo = false)
    @SaCheckPermission("tenant:update")
    @SaCheckSafe
    public R updatePackage(@RequestBody @Validated TenantPackageDTO dto) {
        TenantPackage tenantPackage = AmosUtils.copy(dto, TenantPackage.class);
        tenantPackage.setMenuIds(JsonUtils.objectToJson(dto.getMenuIds()));
        tenantPackageService.saveOrUpdate(tenantPackage);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除租户数据")
    @Log(value = "删除租户数据", logResultInfo = false)
    @SaCheckPermission("tenant:remove")
    @SaCheckSafe
    public R remove(@RequestBody Set<Long> ids) {
        tenantService.remove(ids);
        return R.ok();
    }

    @PostMapping("/package/remove")
    @ApiOperation(value = "删除租户套餐数据")
    @Log(value = "删除租户套餐数据", logResultInfo = false)
    @SaCheckPermission("tenant:remove")
    @SaCheckSafe
    public R removePackage(@RequestBody Set<Long> ids) {
        tenantPackageService.remove(ids);
        return R.ok();
    }
}
