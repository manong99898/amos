
package org.amos.server.modules.upms.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.amos.core.basic.node.TreeNode;

import java.util.List;
import java.util.Objects;

/**
 * 视图实体类
 *
 * @author liubt
 * @since 2022-08-03
 */
@Data
public class DictVO implements TreeNode<Long, DictVO> {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long parentId;
    private String name;
    private String code;
    private Integer type;
    private Integer sort;
    private String remark;
    /**
     * 子孙节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<DictVO> children;

    @Override
    public Long id() {
        return this.getId();
    }

    @Override
    public Long parentId() {
        return this.getParentId();
    }

    @Override
    public boolean root() {
        return Objects.equals(this.getParentId(), 0L);
    }

    @Override
    public void setChildren(List<DictVO> children) {
        this.children = children;
    }
}
