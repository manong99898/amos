package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.core.basic.annotation.CacheBatchEvict;
import org.amos.server.modules.upms.entity.Params;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.Set;

import static org.amos.server.constant.SysCacheConstant.SYS_CONFIG_PARAMS_KEY;

public interface IParamsService extends IService<Params> {
    /**
     * 通过id 获取参数配置信息
     *
     * @param id
     * @return
     */
    @Cacheable(value = SYS_CONFIG_PARAMS_KEY, key = "#id", condition = "#id != null")
    Params findById(Long id);

    /**
     * 更新参数配置信息
     *
     * @param entity
     * @return
     */
    @CacheEvict(value = SYS_CONFIG_PARAMS_KEY, key = "#entity.id", condition = "#entity.id != null")
    Boolean saveOrUpdateConfigParams(Params entity);

    /**
     * 删除参数配置信息
     *
     * @param ids
     * @return
     */
    @CacheBatchEvict(cacheNames = SYS_CONFIG_PARAMS_KEY, key = "#ids")
    Boolean remove(Set<Long> ids);
}