package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.dto.TenantDTO;
import org.amos.server.modules.upms.entity.Tenant;

import java.util.Set;

public interface ITenantService extends IService<Tenant>{
    /**
     * 删除租户
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);
    /**
     * 获取当前线程租户ID
     * @return
     */
    Long getTenantId();
    /**
     * 获取当前线程租户信息
     * @return
     */
    Tenant getTenant();
    /**
     * 通过租户编号查询租户信息
     * @param tenantNo
     * @return
     */
    Tenant getTenantByNo(String tenantNo);
    /**
     * 租户合法性校验
     * @param tenantId
     */
    void valid(Long tenantId);

    /**
     * 新增/更新租户信息
     * @param dto
     * @return
     */
    Boolean saveOrUpdateTenant(TenantDTO dto);
}