package org.amos.server.modules.upms.vo;

import lombok.Data;
import org.amos.core.basic.node.TreeNode;
import org.amos.server.modules.upms.entity.Dept;

import java.util.List;
import java.util.Objects;

/**
 * 部门树形展示vo
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class DeptTreeVO extends Dept implements TreeNode<Long, DeptTreeVO> {
    private List<DeptTreeVO> children;

    @Override
    public Long id() {
        return super.getId();
    }

    @Override
    public Long parentId() {
        return super.getParentId();
    }

    @Override
    public boolean root() {
        return Objects.equals(this.getParentId(), 0L);
    }

    @Override
    public void setChildren(List<DeptTreeVO> children) {
        this.children = children;
    }
}
