package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * 用户状态DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class UserStatusDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 昵称
     */
    @NotNull(message = "用户ID不能为空")
    @MpQuery(column = "id", keyword = SqlKeyWordEnum.IN)
    private Set<Long> ids;

    /**
     * 状态
     */
    @NotNull(message = "用户状态")
    private Integer status;

}
