package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

/**
 * <p>
 * 行政区划表 DTO
 * </p>
 *
 * @author liubt
 * @since 2022-11-10
 */
@Data
public class RegionDTO extends BaseDTO {

    @ApiModelProperty(value = "区域编号")
    private String code;

    @ApiModelProperty(value = "父编号")
    private String parentCode;

    @ApiModelProperty(value = "级联编号")
    private String cascadeCode;

    @ApiModelProperty(value = "全称")
    private String name;

    @ApiModelProperty(value = "省级（省份、直辖市、自治区）编号")
    private String provinceCode;

    @ApiModelProperty(value = "省级（省份、直辖市、自治区）全称")
    private String provinceName;

    @ApiModelProperty(value = "地级（城市）编号")
    private String cityCode;

    @ApiModelProperty(value = "地级（城市）全称")
    private String cityName;

    @ApiModelProperty(value = "县级（区县）编号")
    private String areaCode;

    @ApiModelProperty(value = "县级（区县）全称")
    private String areaName;

    @ApiModelProperty(value = "乡级（乡镇、街道）编号")
    private String streetCode;

    @ApiModelProperty(value = "乡级（乡镇、街道）全称")
    private String streetName;

    @ApiModelProperty(value = "村级（村委会、居委会）编号")
    private String villageCode;

    @ApiModelProperty(value = "村级（村委会、居委会）全称")
    private String villageName;

    @ApiModelProperty(value = "层级（0国、1省、2市、3县、4镇、5村）")
    private Integer level;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

}
