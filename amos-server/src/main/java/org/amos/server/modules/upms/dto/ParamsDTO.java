package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

/**
 * 参数配置表
 */
@Data
public class ParamsDTO extends BaseDTO {
    @ApiModelProperty(value = "参数名称")
    private String name;
    @ApiModelProperty(value = "参数键名")
    private String paramKey;
    @ApiModelProperty(value = "参数键值")
    private String paramValue;
    @ApiModelProperty(value = "状态，1:启用0:禁用")
    private Integer status;
    @ApiModelProperty(value = "备注")
    private String remark;
}