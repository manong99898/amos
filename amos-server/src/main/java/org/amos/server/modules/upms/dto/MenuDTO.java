package org.amos.server.modules.upms.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import org.amos.core.frame.config.jackson.JsonBool2IntDeserializer;

import java.io.Serializable;

/**
 * 菜单DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class MenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    private Long parentId;
    private String name;
    private String path;
    private String redirect;
    private String component;
    private String permission;
    private Integer sort;
    private MetaDTO meta;

    @Data
    public static class MetaDTO {
        private String title;

        private String icon;

        private String type;

        @JsonDeserialize(using = JsonBool2IntDeserializer.class, as = Integer.class, contentAs = boolean.class)
        private Integer hidden;

        @JsonDeserialize(using = JsonBool2IntDeserializer.class, as = Integer.class, contentAs = boolean.class)
        private Integer hiddenBreadcrumb;
    }
}
