/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.core.basic.annotation.CacheBatchEvict;
import org.amos.server.modules.upms.dto.DictDetailDTO;
import org.amos.server.modules.upms.entity.DictDetail;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Set;

import static org.amos.server.constant.SysCacheConstant.SYS_DICT_KEY;

/**
 * <p>
 * 字典具体数据详情 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IDictDetailService extends IService<DictDetail> {
    /**
     * 通过code获取字典信息
     *
     * @param dto
     * @return
     */
    @Cacheable(value = SYS_DICT_KEY, key = "#dto.dictCode", condition = "#dto.dictCode != null")
    List<DictDetail> selectList(DictDetailDTO dto);

    /**
     * 更新字典
     *
     * @param dto
     * @return
     */
    @CacheEvict(value = SYS_DICT_KEY, key = "#dto.dictCode", condition = "#dto.dictCode != null")
    Boolean saveOrUpdateDict(DictDetailDTO dto);

    /**
     * 根据ID删除字典信息
     *
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);

    /**
     * 根据字典ID删除详情信息
     *
     * @param ids
     * @return
     */
    Boolean removeByDictId(Set<Long> ids);

    /**
     * 删除code后缀缓存
     *
     * @param codes
     * @return
     */
    @CacheBatchEvict(cacheNames = SYS_DICT_KEY, key = "#codes")
    Boolean removeByCodes(Set<String> codes);
}
