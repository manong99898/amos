package org.amos.server.modules.upms.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseEntity;

/**
 * 角色vo
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RoleVO extends BaseEntity {

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "别名")
    private String alias;

    @ApiModelProperty(value = "描述")
    private String remark;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty("状态 1:有效 ,0:无效")
    private Integer status;

}
