package org.amos.server.modules.upms.controller;

import cn.hutool.core.lang.Dict;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.vo.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HealthController {

    @GetMapping("/health")
    public R healthCheck() {
        return R.ok(Dict.of("status", "UP"));
    }
}
