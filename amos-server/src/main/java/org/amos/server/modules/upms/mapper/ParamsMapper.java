package org.amos.server.modules.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.amos.server.modules.upms.entity.Params;

public interface ParamsMapper extends BaseMapper<Params> {

}