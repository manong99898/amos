package org.amos.server.modules.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.amos.server.modules.upms.entity.ConfigMsg;

public interface ConfigMsgMapper extends BaseMapper<ConfigMsg> {

}