/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.entity.UserDept;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户部门关联 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IUserDeptService extends IService<UserDept> {
    /**
     * 根据userId获取权限集合
     *
     * @param userId
     * @return
     */
    Set<Long> findDeptIdsByUserId(Long userId);

    /**
     * 更新用户部门信息
     *
     * @param userId
     * @param deptIds
     * @return
     */
    Boolean updateUserDept(Long userId, Set<Long> deptIds);

    /**
     * 批量删除用户部门
     *
     * @param userIds
     * @return
     */
    Boolean removeByUserIds(Set<Long> userIds);
}
