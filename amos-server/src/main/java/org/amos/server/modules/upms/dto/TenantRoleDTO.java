package org.amos.server.modules.upms.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 租户角色DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class TenantRoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色编码
     */
    private String roleCode;
    /**
     * 权限信息
     */
    private Set<Long> permissions;
}
