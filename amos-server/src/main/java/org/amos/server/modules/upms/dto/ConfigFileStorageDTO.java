package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

import java.util.Map;

/**
 * 文件存储配置表
 */
@Data
public class ConfigFileStorageDTO extends BaseDTO {
    @ApiModelProperty(value = "配置编码")
    private String no;
    @ApiModelProperty(value = "配置名")
    private String name;
    @ApiModelProperty(value = "存储平台")
    private Integer platform;
    @ApiModelProperty(value = "存储配置")
    private Map<String, Object> config;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "状态，1:启用0:禁用")
    private Integer status;
    @ApiModelProperty(value = "是否为默认配置")
    private Integer isDefault;
}