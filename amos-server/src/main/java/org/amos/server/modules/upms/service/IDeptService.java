/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.core.basic.annotation.CacheBatchEvict;
import org.amos.server.modules.upms.dto.DeptDTO;
import org.amos.server.modules.upms.entity.Dept;
import org.amos.server.modules.upms.vo.DeptTreeVO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Set;

import static org.amos.server.constant.SysCacheConstant.SYS_DEPT_KEY;

/**
 * <p>
 * 部门 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IDeptService extends IService<Dept> {
    /**
     * 通过id 获取部门信息
     *
     * @param id
     * @return
     */
    @Cacheable(value = SYS_DEPT_KEY, key = "#id", condition = "#id != null")
    Dept findById(Long id);

    /**
     * 更新部门信息
     *
     * @param entity
     * @return
     */
    @CacheEvict(value = SYS_DEPT_KEY, key = "#entity.id", condition = "#entity.id != null")
    Boolean saveOrUpdateDept(Dept entity);

    /**
     * 删除部门信息
     *
     * @param ids
     * @return
     */
    @CacheBatchEvict(cacheNames = SYS_DEPT_KEY, key = "#ids")
    Boolean remove(Set<Long> ids);

    /**
     * 根据ID获取部门信息
     *
     * @param id
     * @return
     */
    Dept getDeptInfo(Long id);

    /**
     * 根据父id获取子集
     *
     * @param id
     * @return
     */
    List<Long> getChildrenByParenId(Long id);

    /**
     * 根据自己列表获取子集ID
     *
     * @param depts
     * @return
     */
    List<Long> getChildrens(List<Dept> depts);

    /**
     * @return
     */
//    @Cacheable(value = SysCacheConstant.SYS_DEPT_LIST_KEY, key = "'tree'")
    List<DeptTreeVO> selectTree(DeptDTO dto);
}
