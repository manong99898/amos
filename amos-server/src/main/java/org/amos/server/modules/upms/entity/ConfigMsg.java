package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.amos.core.basic.base.BaseEntity;
import org.amos.core.frame.config.jackson.JsonStr2MapSerializer;

/**
 *消息配置表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_config_msg", autoResultMap = true)
public class ConfigMsg extends BaseEntity {
    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 配置编码
     */
    private String no;
    /**
     * 配置名
     */
    private String name;
    /**
     * 消息平台
     */
    private Integer platform;
    /**
     * 消息类型 {@link org.amos.msg.enums.MsgTypeEnum}
     */
    private Integer type;
    /**
     * 消息配置
     */
    @JsonSerialize(using = JsonStr2MapSerializer.class)
    private String config;
    /**
     * 备注
     */
    private String remark;
    /**
     * 状态，1:启用0:禁用
     */
    private Integer status;
    /**
     * 是否为默认配置
     */
    private Integer isDefault;
}