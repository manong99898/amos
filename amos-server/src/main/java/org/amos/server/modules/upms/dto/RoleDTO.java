package org.amos.server.modules.upms.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 角色DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    /**
     * 机构名称
     */
    @NotBlank(message = "名称不为空")
    private String name;

    @NotBlank(message = "别名不为空")
    private String alias;

    private String remark;

    /**
     * 排序
     */
    private Integer sort;

    private Integer status;
}
