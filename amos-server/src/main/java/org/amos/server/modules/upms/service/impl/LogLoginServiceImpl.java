package org.amos.server.modules.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.server.modules.upms.entity.Log;
import org.amos.server.modules.upms.mapper.LogLoginMapper;
import org.amos.server.modules.upms.service.ILogLoginService;
import org.springframework.stereotype.Service;

@Service
public class LogLoginServiceImpl extends ServiceImpl<LogLoginMapper, Log> implements ILogLoginService {
	
}