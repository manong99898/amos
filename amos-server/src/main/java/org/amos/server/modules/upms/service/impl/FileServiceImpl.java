package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.amos.core.basic.exception.ServiceException;
import org.amos.filestorage.platform.FilePlatform;
import org.amos.filestorage.utils.FileTypeUtil;
import org.amos.server.modules.upms.service.IConfigFileStorageService;
import org.amos.server.modules.upms.service.IFileService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileServiceImpl implements IFileService {
    private final IConfigFileStorageService configFileStorageService;

    @Override
    public FilePlatform getPlatform(String platformId) {
        return configFileStorageService.getPlatformByConfigNo(platformId);
    }

    @Override
    public Map<String, Object> uploadFile(MultipartFile file, String platformId) {
        String fileName = StrUtil.EMPTY;
        String url = StrUtil.EMPTY;
        String streamUrl = StrUtil.EMPTY;
        try {
            FilePlatform platform = null;
            if (StrUtil.isNotBlank(platformId)) {
                platform = configFileStorageService.getPlatformByConfigNo(platformId);
            }
            if (Objects.isNull(platform)) {
                platform = configFileStorageService.getDefaultPlatform();
            }
            if (Objects.isNull(platform)) {
                throw new ServiceException("请先配置存储平台信息!");
            }

            byte[] bytes = file.getBytes();
            fileName = file.getOriginalFilename();
            String type = FilenameUtils.getExtension(fileName);
            String path = StrUtil.format("{}/{}.{}", DateUtil.format(new Date(), "yyyy/MM/dd"), UUID.randomUUID(), type);
            url = platform.upload(bytes, path, FileTypeUtil.getType(bytes));
            streamUrl = StrUtil.format("/system/file/file-stream/{}/{}", platform.getId(), path);
        } catch (IOException e) {
            throw new ServiceException(e);
        }
        return Dict.of("fileName", fileName, "url", url, "streamUrl", streamUrl);
    }

    @Override
    public Map<String, Object> uploadFileBatch(List<MultipartFile> files, String platformId) {
        List<String> urls = new ArrayList<>();
        List<String> streamUrls = new ArrayList<>();
        try {
            FilePlatform platform = null;
            if (StrUtil.isNotBlank(platformId)) {
                platform = configFileStorageService.getPlatformByConfigNo(platformId);
            }
            if (Objects.isNull(platform)) {
                platform = configFileStorageService.getDefaultPlatform();
            }

            for (MultipartFile file : files) {
                byte[] bytes = file.getBytes();
                String originalFilename = file.getOriginalFilename();
                String type = FilenameUtils.getExtension(originalFilename);
                String path = StrUtil.format("{}/{}.{}", DateUtil.format(new Date(), "yyyy/MM/dd"), UUID.randomUUID(), type);
                String url = platform.upload(bytes, path, FileTypeUtil.getType(bytes));
                String streamUrl = StrUtil.format("/file-stream/{}/{}", platform.getId(), path);
                urls.add(url);
                streamUrls.add(streamUrl);
            }

        } catch (IOException e) {
            throw new ServiceException(e);
        }
        return Dict.of("url", urls, "streamUrl", streamUrls);
    }
}
