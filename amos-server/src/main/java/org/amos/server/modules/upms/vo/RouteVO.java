package org.amos.server.modules.upms.vo;

import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * 路由vo
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RouteVO {
    private List<MenuVO> list;
    private Set<Long> permissions;
}
