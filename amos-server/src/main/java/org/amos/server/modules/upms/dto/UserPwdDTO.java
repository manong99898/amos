package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.base.BaseDTO;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import javax.validation.constraints.NotBlank;

/**
 * 用户密码DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class UserPwdDTO extends BaseDTO {

    /**
     * 用户账号
     */
    @NotBlank(message = "id不为空")
    @MpQuery(column = "id", keyword = SqlKeyWordEnum.EQ)
    private Long id;

    /**
     * 旧密码
     */
    @NotBlank(message = "旧密码不能为空")
    private String oldPwd;

    /**
     * 新密码
     */
    @NotBlank(message = "新密码不能为空")
    private String pwd;
}
