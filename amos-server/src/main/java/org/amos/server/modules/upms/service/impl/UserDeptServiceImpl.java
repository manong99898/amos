/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.server.modules.upms.entity.UserDept;
import org.amos.server.modules.upms.mapper.UserDeptMapper;
import org.amos.server.modules.upms.service.IUserDeptService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户部门关联 服务实现类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Service
public class UserDeptServiceImpl extends ServiceImpl<UserDeptMapper, UserDept> implements IUserDeptService {

    @Override
    public Set<Long> findDeptIdsByUserId(Long userId) {
        QueryWrapper<UserDept> qw = new QueryWrapper<>();
        qw.eq(AmosUtils.toDbField(UserDept::getUserId), userId);
        List<UserDept> userDepts = super.list(qw);

        Set<Long> deptIds = new HashSet<>();
        userDepts.forEach(x -> {
            deptIds.add(x.getDeptId());
        });
        return deptIds;
    }

    @Override
    public Boolean updateUserDept(Long userId, Set<Long> deptIds) {
        //删除用户部门
        this.removeByUserIds(Collections.singleton(userId));
        //批量添加用户部门
        Boolean res = baseMapper.saveBatch(userId, deptIds);
        return res;
    }

    @Override
    public Boolean removeByUserIds(Set<Long> userIds) {
        UpdateWrapper<UserDept> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(UserDept::getUserId), userIds);
        return super.remove(uw);
    }
}
