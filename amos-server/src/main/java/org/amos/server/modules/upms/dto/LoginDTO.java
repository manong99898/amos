package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
@ApiModel("登录认证条件")
public class LoginDTO {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户名", required = true)
    @NotBlank(message = "用户名不能为空")
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "验证码Key", required = true)
    @NotBlank(message = "缺少Key参数")
    private String key;

    @ApiModelProperty(value = "用户输入验证码code", required = true)
    @NotBlank(message = "验证码不能为空")
    private String code;
}
