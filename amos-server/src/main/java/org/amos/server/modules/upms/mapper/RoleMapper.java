/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.amos.server.modules.upms.dto.RoleListDTO;
import org.amos.server.modules.upms.entity.Role;
import org.amos.server.modules.upms.vo.RoleVO;
import org.amos.server.modules.upms.vo.UserVO;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 根据角色ID 和 别名统计总数
     * @param roleIds
     * @param alias
     * @return
     */
    long count(@Param("roleIds") Set<Long> roleIds, String alias);
    /**
     * 角色分页查询
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<RoleVO> selectRolePage(Page<UserVO> page, @Param("ew") Wrapper<RoleListDTO> queryWrapper);
}
