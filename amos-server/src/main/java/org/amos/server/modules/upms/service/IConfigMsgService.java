package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.msg.platform.MsgPlatform;
import org.amos.server.modules.upms.dto.ConfigMsgDTO;
import org.amos.server.modules.upms.entity.ConfigMsg;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.Set;

import static org.amos.server.constant.SysCacheConstant.SYS_CONFIG_SMS_KEY;


public interface IConfigMsgService extends IService<ConfigMsg> {
    /**
     * 获取默认短信平台
     * @param msgType {@link org.amos.msg.enums.MsgTypeEnum}
     * @return
     */
    MsgPlatform getDefaultPlatform(Integer msgType);
    /**
     * 通过id 获取短信配置信息
     *
     * @param id
     * @return
     */
    @Cacheable(value = SYS_CONFIG_SMS_KEY, key = "#id", condition = "#id != null")
    ConfigMsg findById(Long id);

    /**
     * 获取默认配置
     * @param msgType {@link org.amos.msg.enums.MsgTypeEnum}
     * @return
     */
    @Cacheable(value = SYS_CONFIG_SMS_KEY, key = "#result.id", unless = "#result == null")
    ConfigMsg getDefaultConfig(Integer msgType);

    /**
     * 通过编号获取配置信息
     * @param configNo
     * @return
     */
    @Cacheable(value = SYS_CONFIG_SMS_KEY, key = "#configNo", condition = "#configNo != null", unless = "#result == null")
    ConfigMsg getConfigByNo(String configNo);

    /**
     * 更新短信配置信息
     *
     * @param dto
     * @return
     */
    @Caching(evict = {
            @CacheEvict(value = SYS_CONFIG_SMS_KEY, key = "#dto.id", condition = "#dto.id != null"),
            @CacheEvict(value = SYS_CONFIG_SMS_KEY, key = "#dto.no", condition = "#dto.no != null")
    })
    Boolean saveOrUpdateConfig(ConfigMsgDTO dto);

    /**
     * 删除短信配置信息
     *
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);
}