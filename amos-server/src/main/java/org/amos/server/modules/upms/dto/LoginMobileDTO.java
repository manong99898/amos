package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * 用户手机登录DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
@ApiModel(description = "登录认证条件")
public class LoginMobileDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = "手机号不能为空")
    private String mobilePhone;

    @ApiModelProperty(value = "短信验证码", required = true)
    @NotBlank(message = "短信验证码不能为空")
    private String code;
}
