package org.amos.server.modules.upms.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 授权数据vo
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class PermissionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<MenuVO> list;

    private List<Long> permissions;

}
