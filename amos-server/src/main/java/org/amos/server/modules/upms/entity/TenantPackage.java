package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.amos.core.basic.base.BaseEntity;

/**
 * 租户套餐表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_tenant_package")
public class TenantPackage extends BaseEntity {
	/** 
    套餐名
    */
	private String name;
	/**
	 状态,0:禁用 1:启用
    */
	private Integer status;
	/** 
    备注
    */
	private String remark;
	/** 
    关联的菜单编号
    */
	private String menuIds;
}