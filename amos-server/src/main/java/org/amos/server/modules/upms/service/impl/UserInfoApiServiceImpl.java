package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.util.StrUtil;
import org.amos.core.frame.config.mybatis.UserInfoApi;
import org.amos.satoken.domain.bo.AuthInfo;
import org.amos.satoken.utils.UserUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserInfoApiServiceImpl implements UserInfoApi {
    @Override
    public String getCurrentUserName() {
        AuthInfo user = UserUtils.getUser();
        if (Objects.nonNull(user)) {
            return user.getUserName();
        }
        return StrUtil.EMPTY;
    }
}
