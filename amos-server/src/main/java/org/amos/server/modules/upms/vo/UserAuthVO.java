package org.amos.server.modules.upms.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

/**
 * 用户授权信息vo
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class UserAuthVO {

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "是否为admin账号")
    private Integer isAdmin;

    @ApiModelProperty(value = "状态：0未激活、1启用、2锁定")
    private Integer status;

    @ApiModelProperty(value = "角色别名")
    private Set<String> roleAlis;

    @ApiModelProperty(value = "角色ID集合")
    private Set<Long> roleIds;

    @ApiModelProperty(value = "部门ID集合")
    private Set<Long> deptIds;

    @ApiModelProperty(value = "岗位ID集合")
    private Set<Long> postIds;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "岗位名称")
    private String postName;

    @ApiModelProperty(value = "权限集合")
    private Set<String> permissions;
}
