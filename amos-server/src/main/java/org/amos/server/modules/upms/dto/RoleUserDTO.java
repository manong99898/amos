package org.amos.server.modules.upms.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * 用户角色DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RoleUserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "角色ID不能为空")
    private Set<Long> roles;

    @NotNull(message = "用户ID不能为空")
    private Set<Long> ids;
}
