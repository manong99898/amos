/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.annotation.ParamCheck;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.ConfigFileStorageDTO;
import org.amos.server.modules.upms.entity.ConfigFileStorage;
import org.amos.server.modules.upms.service.IConfigFileStorageService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * <p>
 * 文件存储配置表 前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2021-01-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/config/file-storage")
public class ConfigFileStorageController extends BaseController {

    private final IConfigFileStorageService configFileStorageService;

    @GetMapping("/list")
    @ApiOperation(value = "文件存储配置列表信息")
    @Log(value = "文件存储配置列表", logResultInfo = false)
    @SaCheckPermission("config:file:list")
    public R list(ConfigFileStorageDTO dto) {
        IPage<ConfigFileStorage> page = configFileStorageService.page(initPage(dto), buildWrapper(dto));
        return R.ok(page);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新文件存储配置数据")
    @Log(value = "更新文件存储配置数据", logResultInfo = false)
    @SaCheckPermission("config:file:update")
    @SaCheckSafe
    public R update(@RequestBody @Validated ConfigFileStorageDTO dto) {
        configFileStorageService.saveOrUpdateConfigFileStorage(dto);
        return R.ok();
    }

    @PostMapping("/test")
    @ApiOperation(value = "测试文件存储配置的有效性")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("config:file:update")
    public R test(@RequestBody Set<Long> ids) {
        if (!configFileStorageService.test(ids)) {
            return R.error("测试不通过,请检查配置信息!");
        }
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除文件存储配置数据")
    @Log(value = "删除文件存储配置数据", logResultInfo = false)
    @SaCheckPermission("config:file:update")
    @SaCheckSafe
    public R remove(@RequestBody Set<Long> ids) {
        configFileStorageService.remove(ids);
        return R.ok();
    }

}
