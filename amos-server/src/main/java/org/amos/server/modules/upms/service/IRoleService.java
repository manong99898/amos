/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.dto.RoleListDTO;
import org.amos.server.modules.upms.dto.RoleMenuDTO;
import org.amos.server.modules.upms.dto.RoleUserDTO;
import org.amos.server.modules.upms.dto.TenantRoleDTO;
import org.amos.server.modules.upms.entity.Role;
import org.amos.server.modules.upms.entity.User;
import org.amos.server.modules.upms.vo.RoleVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户角色关联 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IRoleService extends IService<Role> {
    /**
     * 保存或更新角色信息
     * @param role
     * @return
     */
    Boolean saveOrUpdateRole(Role role);
    /**
     * 创建租户角色
     *
     * @param dto
     * @return 角色ID
     */
    Long saveTenantRole(TenantRoleDTO dto);
    /**
     * 更新租户角色
     *
     * @param dto
     * @return
     */
    Boolean updateTenantRole(TenantRoleDTO dto);
    /**
     * 删除角色信息
     *
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);

    /**
     * 获取权限信息
     *
     * @param user
     * @return
     */
    Set<String> getUserPermissions(User user);

    /**
     * 角色列表
     * @param dto
     * @return
     */
    List<RoleVO> selectList(RoleListDTO dto);

    /**
     * 设置角色权限
     *
     * @param dto
     * @return
     */
    Boolean resetRolePermission(RoleMenuDTO dto);

    /**
     * 设置角色权限
     *
     * @param dto
     * @return
     */
    Boolean resetRoleUser(RoleUserDTO dto);
}
