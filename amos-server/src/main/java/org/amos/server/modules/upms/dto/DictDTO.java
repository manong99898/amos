package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.base.BaseDTO;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import javax.validation.constraints.NotBlank;

/**
 * 字典DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class DictDTO extends BaseDTO {

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @NotBlank(message = "字典名称不能为空")
    @MpQuery(column = "name", keyword = SqlKeyWordEnum.LIKE)
    private String name;

    @NotBlank(message = "字典编码不能为空")
    private String code;

    @ApiModelProperty(value = "字典类型,0:系统字典1:业务字典")
    private Integer type;

    @ApiModelProperty(value = "状态，1:启用0:禁用")
    private Integer status;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;
}
