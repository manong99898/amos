
package org.amos.server.modules.upms.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.amos.core.basic.node.TreeNode;

import java.util.List;
import java.util.Objects;

/**
 * 视图实体类
 *
 * @author liubt
 * @since 2022-08-03
 */
@Data
public class MenuVO implements TreeNode<Long, MenuVO> {
    private Long id;
    private Long parentId;
    private String name;
    private String path;
    private String component;
    private String permission;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetaVO meta;
    /**
     * 子孙节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<MenuVO> children;

    @Override
    public Long id() {
        return this.getId();
    }

    @Override
    public Long parentId() {
        return this.getParentId();
    }

    @Override
    public boolean root() {
        return Objects.equals(this.getParentId(), 0L);
    }

    @Override
    public void setChildren(List<MenuVO> children) {
        this.children = children;
    }
}
