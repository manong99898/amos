package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

import java.util.List;

/**
 * 租户套餐表
 */
@Data
public class TenantPackageDTO extends BaseDTO {
	@ApiModelProperty(value = "套餐名称")
	private String name;
	@ApiModelProperty(value = "状态,0:禁用 1:启用")
	private Integer status;
	@ApiModelProperty(value = "备注")
	private String remark;
	@ApiModelProperty(value = "关联的菜单编号")
	private List<Long> menuIds;
}