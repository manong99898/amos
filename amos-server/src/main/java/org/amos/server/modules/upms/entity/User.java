package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseEntity;

/**
 * 系统用户
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
@TableName("sys_user")
public class User extends BaseEntity {
    /**
     * 租户ID
     */
    private Long tenantId;
    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "是否为admin账号")
    private Integer isAdmin;

    @ApiModelProperty(value = "状态：0未激活、1启用、2锁定")
    private Integer status;

}
