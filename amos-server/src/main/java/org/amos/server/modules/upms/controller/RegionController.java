package org.amos.server.modules.upms.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.annotation.ParamCheck;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.RegionDTO;
import org.amos.server.modules.upms.service.IRegionService;
import org.amos.server.modules.upms.vo.RegionVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 行政区划表 前端控制器
 * </p>
 *
 * @author liubt
 * @since 2022-11-10
 */
@RestController
@RequestMapping("/system/region")
@AllArgsConstructor
public class RegionController {

    private final IRegionService regionService;

    @GetMapping("/treeList")
    @ApiOperation(value = "行政区划数据树")
    @SaCheckPermission("region:list")
    @Log(value = "行政区划数据树", logResultInfo = false)
    public R treeList(RegionDTO dto) {
        List<RegionVO> list = regionService.selectTree(dto);
        return R.ok(list);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新行政区划数据")
    @SaCheckPermission("region:update")
    @SaCheckSafe
    @Log(value = "更新行政区划数据", logResultInfo = false)
    public R update(@RequestBody @Validated RegionDTO dto) {
        regionService.saveOrUpdateRegion(dto);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除行政区划数据")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("region:update")
    @SaCheckSafe
    @Log(value = "删除行政区划数据", logResultInfo = false)
    public R remove(@RequestBody Set<Long> ids) {
        regionService.removeRegion(ids);
        return R.ok();
    }

}
