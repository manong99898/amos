/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.dto.MenuListDTO;
import org.amos.server.modules.upms.entity.Menu;
import org.amos.server.modules.upms.vo.MenuTreeVO;
import org.amos.server.modules.upms.vo.MenuVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统菜单 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IMenuService extends IService<Menu> {
    /**
     * 删除菜单数据
     *
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);

    /**
     * @param menu
     * @return
     */
    Long saveOrUpdateMenu(Menu menu);

    /**
     * 菜单权限树形结构
     *
     * @param roleIds
     * @return
     */
    List<MenuVO> permissions(Set<Long> roleIds);

    /**
     * 菜单树形结构
     *
     * @param roleIds
     * @return
     */
    List<MenuVO> routes(Set<Long> roleIds);

    /**
     * 根据租户查询菜单(树形)
     *
     * @return
     */
    List<MenuVO> selectTree();

    /**
     * 查询所有菜单(树形)
     *
     * @param dto
     * @return
     */
    List<MenuVO> selectTree(MenuListDTO dto);

    /**
     * 根据用户查询所有接口权限
     *
     * @param userId
     * @return
     */
    Set<String> selectUserPermissions(Long userId);

    /**
     * 根据用户查询所有菜单(树形)
     *
     * @param userId
     * @return
     */
    List<MenuTreeVO> selectUserTree(Long userId);

    /**
     * 根据用户查询所有菜单ID
     *
     * @param userId
     * @return
     */
    List<Long> selectUserTreeID(Long userId);

    /**
     * 根据租户套餐查询所有菜单ID
     *
     * @param packageId 租户套餐ID
     * @return
     */
    List<Long> selectPackageTreeID(Long packageId);

    /**
     * 根据角色查询所有菜单ID
     *
     * @param roleId
     * @return
     */
    List<Long> selectRoleTreeID(Long roleId);

    /**
     * 生成前端页面菜单数据
     *
     * @param menus
     * @return
     */
    List<MenuVO> buildMenus(List<Menu> menus);

    /**
     * 构建菜单树
     *
     * @param menus
     * @return
     */
    List<MenuTreeVO> buildTree(List<Menu> menus);

    /**
     * 获取自己级上级菜单
     *
     * @param id    当前菜单id
     * @param menus
     * @return
     */
    List<Menu> getParentMenus(Long id, List<Menu> menus);

    /**
     * 获取当前用户菜单数据
     *
     * @param userId
     * @param menuType
     * @return
     */
    List<Menu> getUserMenu(Long userId, Integer menuType);

    /**
     * 刷新当前用户菜单
     *
     * @param ids
     * @return
     */
//    @CacheBatchEvict(cacheNames={})
    Boolean updateMenu(Set<Long> ids);
}
