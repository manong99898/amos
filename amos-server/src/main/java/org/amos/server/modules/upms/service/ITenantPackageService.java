package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.entity.TenantPackage;

import java.util.Set;

public interface ITenantPackageService extends IService<TenantPackage>{
    /**
     * 删除租户
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);
    /**
     * 根据套餐ID 获取租户套餐
     * @param packageId
     * @return
     */
    TenantPackage selectById(Long packageId);
}