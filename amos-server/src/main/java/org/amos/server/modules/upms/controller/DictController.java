/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.annotation.ParamCheck;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.DictDTO;
import org.amos.server.modules.upms.dto.DictDetailDTO;
import org.amos.server.modules.upms.entity.Dict;
import org.amos.server.modules.upms.entity.DictDetail;
import org.amos.server.modules.upms.service.IDictDetailService;
import org.amos.server.modules.upms.service.IDictService;
import org.amos.server.modules.upms.vo.DictVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 数据字典 前端控制器
 *
 * @author liubt
 * @since 2020-12-20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/dict")
public class DictController extends BaseController {

    private final IDictService dictService;
    private final IDictDetailService dictDetailService;

    @GetMapping("/list")
    @ApiOperation(value = "字典数据列表")
    @SaCheckPermission("dict:list")
    @Log(value = "字典数据列表", logResultInfo = false)
    public R list(DictDTO dto) {
        IPage<Dict> page = dictService.page(initPage(dto), buildWrapper(dto));
        return R.ok(page);
    }

    @GetMapping("/treeList")
    @ApiOperation(value = "字典数据树")
    @SaCheckPermission("dict:list")
    @Log(value = "字典数据树", logResultInfo = false)
    public R treeList(DictDTO dto) {
        List<DictVO> list = dictService.selectTree(dto);
        return R.ok(list);
    }

    @GetMapping("/detail/list")
    @ApiOperation(value = "字典数据列表(字典详情)")
    @Log(value = "字典数据列表(字典详情)", logResultInfo = false)
    public R detailList(DictDetailDTO dto) {
        List<DictDetail> list = dictDetailService.selectList(dto);
        return R.ok(list);
    }
    @PostMapping("/update")
    @ApiOperation(value = "更新字典数据")
    @SaCheckPermission("dict:update")
    @SaCheckSafe
    @Log(value = "更新字典数据", logResultInfo = false)
    public R update(@RequestBody @Validated DictDTO dto) {
        dictService.saveOrUpdateDict(dto);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除字典数据")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("dict:update")
    @SaCheckSafe
    @Log(value = "删除字典数据", logResultInfo = false)
    public R remove(@RequestBody Set<Long> ids) {
        dictService.remove(ids);
        return R.ok();
    }

    @PostMapping("/detail/update")
    @ApiOperation(value = "更新字典数据(字典详情)")
    @SaCheckPermission("dict:update")
    @SaCheckSafe
    @Log(value = "更新字典数据(字典详情)", logResultInfo = false)
    public R detailUpdate(@RequestBody @Validated DictDetailDTO dto) {
        dictDetailService.saveOrUpdateDict(dto);
        return R.ok();
    }

    @PostMapping("/detail/update-batch")
    @ApiOperation(value = "更新字典数据(字典详情)")
    @SaCheckPermission("dict:update")
    @SaCheckSafe
    @Log(value = "批量更新字典数据(字典详情)", logResultInfo = false)
    public R detailUpdateBatch(@RequestBody @Validated List<DictDetailDTO> list) {
        for (DictDetailDTO dto : list) {
            dto.setSort(list.indexOf(dto) + 1);
            dictDetailService.saveOrUpdateDict(dto);
        }
        return R.ok();
    }

    @PostMapping("/detail/remove")
    @ApiOperation(value = "删除字典数据(字典详情)")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("dict:update")
    @SaCheckSafe
    @Log(value = "删除字典数据(字典详情)", logResultInfo = false)
    public R detailRemove(@RequestBody Set<Long> ids) {
        UpdateWrapper<DictDetail> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(DictDetail::getId), ids).set(AmosUtils.toDbField(DictDetail::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        dictDetailService.update(uw);
        return R.ok();
    }
}
