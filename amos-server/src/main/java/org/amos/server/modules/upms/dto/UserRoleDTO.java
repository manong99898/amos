package org.amos.server.modules.upms.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long userId;
    private Long roleId;
    private String roleName;
}
