/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.vo.R;
import org.amos.msg.enums.MsgTypeEnum;
import org.amos.server.modules.upms.dto.ConfigMsgDTO;
import org.amos.server.modules.upms.entity.ConfigMsg;
import org.amos.server.modules.upms.service.IConfigMsgService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * <p>
 * 微信配置前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2021-01-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/config/wx")
public class ConfigWxController extends BaseController {

    private final IConfigMsgService configMsgService;

    @GetMapping("/list")
    @ApiOperation(value = "微信配置列表信息")
    @Log(value = "微信配置列表信息", logResultInfo = false)
    @SaCheckPermission("config:wx:list")
    public R list(ConfigMsgDTO dto) {
        dto.setType(MsgTypeEnum.WX.getType());
        IPage<ConfigMsg> page = configMsgService.page(initPage(dto), buildWrapper(dto));
        return R.ok(page);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新微信配置数据")
    @Log(value = "更新微信配置数据", logResultInfo = false)
    @SaCheckPermission("config:wx:update")
    @SaCheckSafe
    public R update(@RequestBody @Validated ConfigMsgDTO dto) {
        dto.setType(MsgTypeEnum.WX.getType());
        configMsgService.saveOrUpdateConfig(dto);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除微信配置数据")
    @Log(value = "删除微信配置数据", logResultInfo = false)
    @SaCheckPermission("config:wx:update")
    @SaCheckSafe
    public R remove(@RequestBody Set<Long> ids) {
        configMsgService.remove(ids);
        return R.ok();
    }
}
