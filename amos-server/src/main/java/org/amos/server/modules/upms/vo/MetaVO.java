package org.amos.server.modules.upms.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.amos.core.frame.config.jackson.JsonInt2BoolSerializer;

/**
 * @desc: 路由元数据
 * @author: liubt
 * @date: 2022-08-03 13:48
 **/
@Data
public class MetaVO {

    private String title;

    private String icon;

    private String type;

    @JsonSerialize(using = JsonInt2BoolSerializer.class)
    private Integer hidden;

    @JsonSerialize(using = JsonInt2BoolSerializer.class)
    private Integer hiddenBreadcrumb;
}
