package org.amos.server.modules.upms.mapper;

import org.amos.server.modules.upms.entity.Region;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 行政区划表 Mapper 接口
 * </p>
 *
 * @author liubt
 * @since 2022-11-10
 */
public interface RegionMapper extends BaseMapper<Region> {

}
