package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.amos.core.basic.base.BaseEntity;

/**
 * 系统日志
 */
@Data
@TableName("sys_log")
public class Log extends BaseEntity {
    /**
     * 操作类
     */
    private String clazz;
    /**
     * 返回信息
     */
    private String resultInfo;
    /**
     * ip
     */
    private String ip;
    /**
     * 日志级别
     */
    private String level;
    /**
     * 操作方法
     */
    private String method;
    /**
     * 日志名称
     */
    private String name;
    /**
     * 请求参数
     */
    private String params;
    /**
     * 链路ID
     */
    private String traceId;
    /**
     * 耗时
     */
    private Long time;
}