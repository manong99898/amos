package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.filestorage.platform.FilePlatform;
import org.amos.server.modules.upms.dto.ConfigFileStorageDTO;
import org.amos.server.modules.upms.entity.ConfigFileStorage;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.Set;

import static org.amos.server.constant.SysCacheConstant.SYS_CONFIG_FILE_STORAGE_KEY;

public interface IConfigFileStorageService extends IService<ConfigFileStorage> {
    /**
     * 获取默认文件存储平台
     * @return
     */
    FilePlatform getDefaultPlatform();
    /**
     * 通过配置编号获取文件存储平台
     * @return
     */
    FilePlatform getPlatformByConfigNo(String configNo);
    /**
     * 通过id 获取文件配置信息
     *
     * @param id
     * @return
     */
    @Cacheable(value = SYS_CONFIG_FILE_STORAGE_KEY, key = "#id", condition = "#id != null", unless = "#result == null")
    ConfigFileStorage findById(Long id);

    /**
     * 获取默认配置
     * @return
     */
    @Cacheable(value = SYS_CONFIG_FILE_STORAGE_KEY, key = "#result.id", unless = "#result == null")
    ConfigFileStorage getDefaultConfig();

    /**
     * 通过编号获取配置信息
     * @param configNo
     * @return
     */
    @Cacheable(value = SYS_CONFIG_FILE_STORAGE_KEY, key = "#configNo", condition = "#configNo != null", unless = "#result == null")
    ConfigFileStorage getConfigByNo(String configNo);

    /**
     * 更新参数配置信息
     *
     * @param dto
     * @return
     */
    @Caching(evict = {
            @CacheEvict(value = SYS_CONFIG_FILE_STORAGE_KEY, key = "#dto.id", condition = "#dto.id != null"),
            @CacheEvict(value = SYS_CONFIG_FILE_STORAGE_KEY, key = "#dto.no", condition = "#dto.no != null")
    })
    Boolean saveOrUpdateConfigFileStorage(ConfigFileStorageDTO dto);

    /**
     * 测试参数配置信息
     *
     * @param ids
     * @return
     */
    Boolean test(Set<Long> ids);

    /**
     * 删除参数配置信息
     *
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);
}