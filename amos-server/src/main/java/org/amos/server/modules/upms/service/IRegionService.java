package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.dto.RegionDTO;
import org.amos.server.modules.upms.entity.Region;
import org.amos.server.modules.upms.vo.RegionVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 行政区划表 服务类
 * </p>
 *
 * @author liubt
 * @since 2022-11-10
 */
public interface IRegionService extends IService<Region> {
    /**
     * 查询行政区划树
     *
     * @param dto
     * @return
     */
    List<RegionVO> selectTree(RegionDTO dto);

    /**
     * 更新行政区划信息
     *
     * @param dto
     */
    void saveOrUpdateRegion(RegionDTO dto);

    /**
     * 删除信息
     *
     * @param ids
     */
    void removeRegion(Set<Long> ids);
}
