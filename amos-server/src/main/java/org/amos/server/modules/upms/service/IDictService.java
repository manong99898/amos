/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.server.modules.upms.dto.DictDTO;
import org.amos.server.modules.upms.entity.Dict;
import org.amos.server.modules.upms.vo.DictVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 数据字典 服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface IDictService extends IService<Dict> {
    /**
     * @param dto
     * @return
     */
    List<DictVO> selectTree(DictDTO dto);

    /**
     * 通过ID获取字典信息
     *
     * @param id
     * @return
     */
    Dict findById(Long id);

    /**
     * 更新字典
     *
     * @param dto
     * @return
     */
    Boolean saveOrUpdateDict(DictDTO dto);

    /**
     * 删除字典信息
     *
     * @param ids
     * @return
     */
    Boolean remove(Set<Long> ids);
}
