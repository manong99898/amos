package org.amos.server.modules.upms.service;

import org.amos.filestorage.platform.FilePlatform;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface IFileService {
    /**
     * 根据文件存储平台唯一标识获取 FilePlatform
     * @param platformId
     * @return
     */
    FilePlatform getPlatform(String platformId);
    /**
     * 文件上传
     * @param file
     * @param platformId
     * @return
     */
    Map<String, Object> uploadFile(MultipartFile file, String platformId);
    /**
     * 批量文件上传
     * @param file
     * @param platformId
     * @return
     */
    Map<String, Object> uploadFileBatch(List<MultipartFile> files, String platformId);
}
