//package org.amos.admin.modules.api.service;
//
//import lombok.RequiredArgsConstructor;
//import org.amos.admin.modules.system.entity.AmosUser;
//import org.amos.admin.modules.system.service.IRoleService;
//import org.amos.admin.modules.system.service.IUserService;
//import org.amos.core.basic.utils.AmosUtils;
//import org.springframework.stereotype.Service;
//
//import java.util.Objects;
//import java.util.Set;
//
///**
// * @desc: 权限查询相关
// * @author: liubt
// * @date: 2022-08-08 14:31
// **/
//@Service
//@RequiredArgsConstructor
//public class PermissionService implements PermissionApi {
//    private final IUserService userService;
//    private final IRoleService roleService;
//    @Override
//    public LoginUser findByName(String userName) {
//        AmosUser amosUser = userService.findByName(userName);
//        if (Objects.isNull(amosUser)){
//            return new LoginUser();
//        }
//        return AmosUtils.copy(amosUser, LoginUser.class);
//    }
//
//    @Override
//    public Set<String> getUserPermissions(LoginUser user) {
//        return roleService.getUserPermissions(AmosUtils.copy(user, AmosUser.class));
//    }
//}
