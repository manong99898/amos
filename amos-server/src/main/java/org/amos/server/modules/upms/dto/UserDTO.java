package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.base.BaseDTO;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import javax.validation.constraints.NotBlank;
import java.util.Set;

/**
 * 用户DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class UserDTO extends BaseDTO {

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 用户账号
     */
    @NotBlank(message = "用户账号不为空")
    @MpQuery(column = "user_name", keyword = SqlKeyWordEnum.LIKE)
    private String userName;
    /**
     * 昵称
     */
    @NotBlank(message = "用户名不为空")
    @MpQuery(column = "nick_name", keyword = SqlKeyWordEnum.LIKE)
    private String nickName;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 用户手机号
     */
    @MpQuery(column = "phone", keyword = SqlKeyWordEnum.LIKE)
    private String phone;

    /**
     * 性别
     */
    @MpQuery(column = "sex", keyword = SqlKeyWordEnum.EQ)
    private Integer sex;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户状态
     */
    private Integer status;

    /**
     * 邮箱
     */
    @MpQuery(column = "email", keyword = SqlKeyWordEnum.LIKE)
    private String email;

    /**
     * 是否为管理员
     */
    @MpQuery(column = "is_admin", keyword = SqlKeyWordEnum.EQ)
    private Integer isAdmin;

    private Long deptId;

    /**
     * 所属组织
     */
    private Set<Long> deptIds;

    /**
     * 所属岗位
     */
    private Set<Long> postIds;

    /**
     * 角色id
     */
    private Set<Long> roleIds;

    /**
     * 权限id
     */
    private Set<Long> permissionIds;
}
