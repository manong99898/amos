package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 区域拖拽DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RegionDragDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "ids不能为空")
    @ApiModelProperty(value = "菜单id数组")
    private List<Long> ids;

    @ApiModelProperty(value = "菜单父id数组")
    private Long parentId;
}
