package org.amos.server.modules.upms.vo;

import lombok.Data;
import org.amos.core.basic.node.TreeNode;
import org.amos.server.modules.upms.entity.Menu;

import java.util.List;
import java.util.Objects;

/**
 * @desc: 菜单树（完整菜单数据）
 * @author: liubt
 * @date: 2022-08-09 13:56
 **/
@Data
public class MenuTreeVO extends Menu implements TreeNode<Long, MenuTreeVO> {

    private List<MenuTreeVO> children;

    @Override
    public Long id() {
        return super.getId();
    }

    @Override
    public Long parentId() {
        return super.getParentId();
    }

    @Override
    public boolean root() {
        return Objects.equals(this.getParentId(), 0L);
    }

    @Override
    public void setChildren(List<MenuTreeVO> children) {
        this.children = children;
    }

    @Override
    public List<MenuTreeVO> getChildren() {
        return this.children;
    }
}
