/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.server.modules.upms.entity.UserPost;
import org.amos.server.modules.upms.mapper.UserPostMapper;
import org.amos.server.modules.upms.service.IUserPostService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 用户岗位关联 服务实现类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Service
public class UserPostServiceImpl extends ServiceImpl<UserPostMapper, UserPost> implements IUserPostService {

    @Override
    public Set<Long> findPostsByUserId(Long userId) {
        QueryWrapper<UserPost> qw = new QueryWrapper<>();
        qw.eq(AmosUtils.toDbField(UserPost::getUserId), userId);
        List<UserPost> userPosts = super.list(qw);

        Set<Long> resultPosts = new HashSet<>();
        userPosts.forEach(userPost -> {
            resultPosts.add(userPost.getPostId());
        });
        return resultPosts;
    }

    @Override
    public Boolean updateUserPost(Long userId, Set<Long> postIds) {
        //删除用户岗位
        this.removeByUserIds(Collections.singleton(userId));
        //批量添加用户岗位
        Boolean res = baseMapper.saveBatch(userId, postIds);
        return res;
    }

    @Override
    public Boolean removeByUserIds(Set<Long> userIds) {
        UpdateWrapper<UserPost> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(UserPost::getUserId), userIds);
        return super.remove(uw);
    }
}
