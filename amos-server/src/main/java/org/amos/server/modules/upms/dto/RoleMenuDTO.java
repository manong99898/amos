package org.amos.server.modules.upms.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * 角色菜单DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RoleMenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "角色ID不能为空")
    private Long roleId;

    @NotNull(message = "权限信息不能为空")
    private Set<Long> permissions;
}
