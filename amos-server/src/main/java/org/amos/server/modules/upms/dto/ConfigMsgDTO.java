package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.base.BaseDTO;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import java.util.Map;

/**
 * 消息配置表
 */
@Data
public class ConfigMsgDTO extends BaseDTO {
    @ApiModelProperty(value = "配置编码")
    private String no;
    @ApiModelProperty(value = "配置名")
    private String name;
    @ApiModelProperty(value = "消息平台")
    private Integer platform;
    @ApiModelProperty(value = "消息配置")
    private Map<String, Object> config;
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     * {@link org.amos.msg.enums.MsgTypeEnum}
     */
    @ApiModelProperty(value = "消息类型")
    @MpQuery(column = "type",keyword = SqlKeyWordEnum.EQ)
    private Integer type;
    @ApiModelProperty(value = "状态，1:启用0:禁用")
    private Integer status;
    @ApiModelProperty(value = "是否为默认配置")
    private Integer isDefault;
}