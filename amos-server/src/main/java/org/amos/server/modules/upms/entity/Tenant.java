package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.amos.core.basic.base.BaseEntity;

import java.util.Date;

/**
 * 租户表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_tenant")
public class Tenant extends BaseEntity {
	/**
	 租户编号
	 */
	private String no;
	/**
	 /**
    租户名
    */
	private String name;
	/** 
    联系人的用户编号
    */
	private Long contactUserId;
	/** 
    联系人
    */
	private String contactName;
	/** 
    联系手机
    */
	private String contactMobile;
	/**
	 状态,0:禁用 1:启用
    */
	private Integer status;
	/** 
    绑定域名
    */
	private String domain;
	/** 
    租户套餐编号
    */
	private Long packageId;
	/** 
    过期时间
    */
	private Date expireTime;
	/** 
    账号数量
    */
	private Long accountCount;
}