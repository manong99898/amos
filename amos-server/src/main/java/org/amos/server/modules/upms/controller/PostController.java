/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.PostDTO;
import org.amos.server.modules.upms.entity.Post;
import org.amos.server.modules.upms.service.IPostService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 岗位表 前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2021-01-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/post")
public class PostController extends BaseController {

    private final IPostService postService;

    @GetMapping("/list")
    @ApiOperation(value = "岗位列表信息")
    @Log(value = "岗位列表信息", logResultInfo = false)
    @SaCheckPermission("post:list")
    public R list(PostDTO dto) {
        List<Post> list = postService.list(buildWrapper(dto));
        return R.ok(list);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新岗位数据")
    @Log(value = "更新岗位数据", logResultInfo = false)
    @SaCheckPermission("post:update")
    public R update(@RequestBody @Validated PostDTO dto) {
        Post dept = AmosUtils.copy(dto, Post.class);
        postService.saveOrUpdate(dept);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除岗位数据")
    @Log(value = "删除岗位数据", logResultInfo = false)
    @SaCheckPermission("post:update")
    public R remove(@RequestBody Set<Long> ids) {
        UpdateWrapper<Post> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(Post::getId), ids).set(AmosUtils.toDbField(Post::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        postService.update(uw);
        return R.ok();
    }
}
