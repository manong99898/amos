package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.base.BaseDTO;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 字典详情DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class DictDetailDTO extends BaseDTO {

    @MpQuery(column = "dict_id", keyword = SqlKeyWordEnum.EQ)
    private Long dictId;

    @NotBlank(message = "字典code不能为空")
    @ApiModelProperty("字典code")
    @MpQuery(column = "dict_code", keyword = SqlKeyWordEnum.EQ)
    private String dictCode;

    @NotBlank(message = "字典名称不能为空")
    @MpQuery(column = "name", keyword = SqlKeyWordEnum.LIKE)
    @ApiModelProperty("字典名称")
    private String name;

    @NotNull(message = "字典值不能为空")
    @ApiModelProperty("字典值")
    private Integer value;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("状态 1:有效 ,0:无效")
    @NotNull(message = "字典状态不能为空")
    private Integer status;

    @MpQuery(column = "sort", keyword = SqlKeyWordEnum.ASC)
    @ApiModelProperty("排序")
    private Integer sort;

}
