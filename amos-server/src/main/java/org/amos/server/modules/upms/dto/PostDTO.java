package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PostDTO extends BaseDTO {

    @NotBlank(message = "岗位名称不为空")
    private String name;


    @ApiModelProperty(value = "岗位描述")
    private String remark;

    @NotNull(message = "岗位排序不能为空")
    private Integer sort;

    private Integer status;
}
