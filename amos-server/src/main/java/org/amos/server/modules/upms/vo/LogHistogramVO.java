package org.amos.server.modules.upms.vo;

import lombok.Data;

import java.util.List;

@Data
public class LogHistogramVO {
    private List<String> dateList;
    private List<Long> infoList;
    private List<Long> errorList;
}
