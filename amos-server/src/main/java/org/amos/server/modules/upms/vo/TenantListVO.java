package org.amos.server.modules.upms.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 租户表
 */
@Data
public class TenantListVO {
	@ApiModelProperty(value = "租户编号")
	@JsonProperty("value")
	private String no;
	@ApiModelProperty(value = "租户名")
	private String name;
}