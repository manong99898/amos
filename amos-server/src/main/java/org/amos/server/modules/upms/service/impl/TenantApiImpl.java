package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.lang.Assert;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.amos.server.modules.upms.entity.Tenant;
import org.amos.server.modules.upms.service.ITenantService;
import org.amos.tenant.core.TenantApi;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TenantApiImpl implements TenantApi {

    private final ITenantService tenantService;
    @Override
    public void valid(Long tenantId) {
        tenantService.valid(tenantId);
    }

    @Override
    public Long getTenantId(String tenantNo) {
        Tenant tenant = tenantService.getTenantByNo(tenantNo);
        Assert.notNull(tenant, "tenant information is null");
        return tenant.getId();
    }
}
