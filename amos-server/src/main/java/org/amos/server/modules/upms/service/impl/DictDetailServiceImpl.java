/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.utils.SpringUtils;
import org.amos.core.basic.utils.crud.WrapperBuilder;
import org.amos.server.modules.upms.dto.DictDetailDTO;
import org.amos.server.modules.upms.entity.DictDetail;
import org.amos.server.modules.upms.mapper.DictDetailMapper;
import org.amos.server.modules.upms.service.IDictDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 字典具体数据详情 服务实现类
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Service
public class DictDetailServiceImpl extends ServiceImpl<DictDetailMapper, DictDetail> implements IDictDetailService {

    @Override
    public List<DictDetail> selectList(DictDetailDTO dto) {
        QueryWrapper<DictDetail> queryWrapper = new WrapperBuilder().build(dto);
        return super.list(queryWrapper);
    }

    @Override
    @Log(value = "[更新字典详情]")
    public Boolean saveOrUpdateDict(DictDetailDTO dto) {
        DictDetail dict = AmosUtils.copy(dto, DictDetail.class);
        return super.saveOrUpdate(dict);
    }

    @Override
    @Log(value = "[根据ID删除字典详情信息]")
    @Transactional(rollbackFor = Exception.class)
    public Boolean remove(Set<Long> ids) {
        UpdateWrapper<DictDetail> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in(AmosUtils.toDbField(DictDetail::getId), ids).set(AmosUtils.toDbField(DictDetail::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        super.update(updateWrapper);
        // 删除字典详情缓存
        QueryWrapper<DictDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.in(AmosUtils.toDbField(DictDetail::getId), ids);
        List<DictDetail> list = super.list(queryWrapper);
        if (CollUtil.isNotEmpty(list)) {
            Set<String> codeSet = list.stream().map(DictDetail::getDictCode).collect(Collectors.toSet());
            SpringUtils.getBean(this.getClass()).removeByCodes(codeSet);
        }
        return Boolean.TRUE;
    }

    @Override
    @Log(value = "[根据字典ID删除详情信息]")
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeByDictId(Set<Long> ids) {
        UpdateWrapper<DictDetail> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in(AmosUtils.toDbField(DictDetail::getDictId), ids).set(AmosUtils.toDbField(DictDetail::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        super.update(updateWrapper);

        // 删除字典详情缓存
        QueryWrapper<DictDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.in(AmosUtils.toDbField(DictDetail::getDictId), ids);
        List<DictDetail> list = super.list(queryWrapper);
        if (CollUtil.isNotEmpty(list)) {
            Set<String> codeSet = list.stream().map(DictDetail::getDictCode).collect(Collectors.toSet());
            SpringUtils.getBean(this.getClass()).removeByCodes(codeSet);
        }
        return Boolean.TRUE;
    }

    @Override
    @Log(value = "[删除字典code后缀缓存]")
    public Boolean removeByCodes(Set<String> codes) {
        return Boolean.TRUE;
    }
}