/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.amos.server.modules.upms.dto.UserDeptDTO;
import org.amos.server.modules.upms.entity.UserDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户部门关联 Mapper 接口
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface UserDeptMapper extends BaseMapper<UserDept> {
    /**
     * 根据用户ID 获取相关联部门信息
     * @param userIds
     * @return
     */
    List<UserDeptDTO> selectDepts(@Param("userIds") Set<Long> userIds);

    /**
     * 批量添加用户部门关联
     * @param userId
     * @param deptIds
     * @return
     */
    Boolean saveBatch(Long userId, Set<Long> deptIds);
}
