/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.utils.TreeUtils;
import org.amos.core.basic.utils.crud.WrapperBuilder;
import org.amos.server.modules.upms.dto.DictDTO;
import org.amos.server.modules.upms.entity.Dict;
import org.amos.server.modules.upms.mapper.DictMapper;
import org.amos.server.modules.upms.service.IDictDetailService;
import org.amos.server.modules.upms.service.IDictService;
import org.amos.server.modules.upms.vo.DictVO;
import org.amos.server.utils.SysTreeUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * 数据字典 服务实现类
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Service
@RequiredArgsConstructor
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {
    private final IDictDetailService dictDetailService;
    private final SysTreeUtils<Dict> sysTreeUtils;

    @Override
    public List<DictVO> selectTree(DictDTO dto) {
        List<Dict> dictList = baseMapper.selectList(new WrapperBuilder().build(dto));
        if (CollUtil.isEmpty(dictList)) {
            return new ArrayList<>();
        }
        List<DictVO> originalList = AmosUtils.listCopy(dictList, DictVO.class);
        return TreeUtils.generateTrees(originalList);
    }

    @Override
    public Dict findById(Long id) {
        if (Objects.isNull(id)) {
            return null;
        }
        return super.getById(id);
    }

    @Override
    @Log(value = "[更新字典]")
    public Boolean saveOrUpdateDict(DictDTO dto) {
        if (ObjectUtil.isEmpty(dto.getParentId())) {
            dto.setParentId(SystemConstant.SYS_TREE_ROOT_ID);
        }
        Dict dict = AmosUtils.copy(dto, Dict.class);
        return super.saveOrUpdate(dict);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Log(value = "[删除字典信息]")
    public Boolean remove(Set<Long> ids) {
        List<Long> dictIds = new ArrayList<>();
        // 组装子集id
        ids.forEach(x -> {
            List<Long> childrenDictIds = sysTreeUtils.getChildrenIds(x, new ArrayList<>(), DictMapper.class);
            dictIds.addAll(childrenDictIds);
        });
        // 合并
        dictIds.addAll(ids);
        UpdateWrapper<Dict> dictUpdateWrapper = new UpdateWrapper<>();
        dictUpdateWrapper.in(AmosUtils.toDbField(Dict::getId), dictIds).set(AmosUtils.toDbField(Dict::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        super.update(dictUpdateWrapper);
        dictDetailService.removeByDictId(ids);
        return Boolean.TRUE;
    }
}
