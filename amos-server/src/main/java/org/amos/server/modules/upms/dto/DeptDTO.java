package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

import javax.validation.constraints.NotBlank;

/**
 * 部门DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class DeptDTO extends BaseDTO {

    /**
     * 父id
     */
//    @NotNull(message = "父id不为空")
    private Long parentId;

    /**
     * 机构名称
     */
    @NotBlank(message = "名称不为空")
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    private Integer type;

    private Integer status;
}
