package org.amos.server.modules.upms.vo;

import lombok.Data;
import org.amos.server.modules.upms.dto.RoleDTO;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 用户角色vo
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class UserRoleVO implements Serializable {

    private static final long serialVersionUID = 1L;

    List<RoleDTO> roles;

    Set<Long> ids;

}
