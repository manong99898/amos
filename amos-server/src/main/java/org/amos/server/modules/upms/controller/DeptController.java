/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.annotation.ParamCheck;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.DeptDTO;
import org.amos.server.modules.upms.entity.Dept;
import org.amos.server.modules.upms.service.IDeptService;
import org.amos.server.modules.upms.vo.DeptTreeVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 部门 前端控制器
 *
 * @author liubt
 * @since 2020-12-20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/dept")
public class DeptController extends BaseController {

    private final IDeptService deptService;

    @GetMapping("/list")
    @ApiOperation(value = "部门列表信息")
    @SaCheckPermission("dept:list")
    @Log(value = "部门列表信息", logResultInfo = false)
    public R list(DeptDTO dto) {
        List<DeptTreeVO> voList = deptService.selectTree(dto);
        return R.ok(voList);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新部门数据")
    @SaCheckPermission("dept:update")
    @Log(value = "更新部门数据", logResultInfo = false)
    public R update(@RequestBody @Validated DeptDTO dto) {
        Dept dept = AmosUtils.copy(dto, Dept.class);
        deptService.saveOrUpdateDept(dept);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除部门数据")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("dept:update")
    @SaCheckSafe
    @Log(value = "删除部门数据", logResultInfo = false)
    public R remove(@RequestBody Set<Long> ids) {
        deptService.remove(ids);
        return R.ok();
    }
}
