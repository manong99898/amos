package org.amos.server.modules.upms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.core.basic.annotation.CacheBatchEvict;
import org.amos.server.modules.upms.dto.UserDTO;
import org.amos.server.modules.upms.dto.UserStatusDTO;
import org.amos.server.modules.upms.entity.User;
import org.amos.server.modules.upms.vo.UserVO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.Set;

import static org.amos.server.constant.SysCacheConstant.SYS_USER_KEY;

public interface IUserService extends IService<User> {
    /**
     * 根据手机号查找用户
     *
     * @param mobile
     * @return
     */
    User getUserByMobile(String mobile);
    /**
     * 根据账号、密码查找用户
     *
     * @param account
     * @return
     */
    User getUserByAccount(String account);

    /**
     * 根据账号查找用户
     *
     * @param name
     * @return
     */
    User findByName(String name);

    /**
     * 根据id查找用户
     *
     * @param id
     * @return
     */
    @Cacheable(value = SYS_USER_KEY, key = "#id")
    User findById(Long id);

    /**
     * 修改用户
     *
     * @param dto
     * @return userId
     */
    @CacheEvict(value = SYS_USER_KEY, key = "#dto.id", condition = "#dto.id != null")
    Long saveOrUpdateUser(UserDTO dto);

    /**
     * 重置用户密码
     *
     * @param ids
     * @return
     */
    @CacheBatchEvict(cacheNames = SYS_USER_KEY, key = "#ids")
    Boolean resetPwd(Set<Long> ids);

    /**
     * 设置用户状态
     *
     * @param dto
     * @return
     */
    @CacheBatchEvict(cacheNames = SYS_USER_KEY, key = "#dto.ids")
    Boolean setStatus(UserStatusDTO dto);

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    @CacheBatchEvict(cacheNames = SYS_USER_KEY, key = "#ids")
    Boolean removeUser(Set<Long> ids);

    /**
     * 查询所有用户信息
     *
     * @param page
     * @param dto
     * @return
     */
    IPage<UserVO> selectUserPage(Page<User> page, UserDTO dto);

}
