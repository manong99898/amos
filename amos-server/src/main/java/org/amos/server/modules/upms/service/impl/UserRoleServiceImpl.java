/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.server.modules.upms.entity.Role;
import org.amos.server.modules.upms.entity.UserRole;
import org.amos.server.modules.upms.mapper.UserRoleMapper;
import org.amos.server.modules.upms.service.IRoleService;
import org.amos.server.modules.upms.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 用户角色关联 服务实现类
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {
    @Autowired
    private IRoleService roleService;

    @Override
    public Set<Role> findRolesByUserId(Long userId) {
        Set<Long> roleIds = this.findRoleIdsByUserId(userId);
        if (CollUtil.isEmpty(roleIds)) {
            return new HashSet<>();
        }
        QueryWrapper<Role> qw = new QueryWrapper();
        qw.in(AmosUtils.toDbField(Role::getId), roleIds);
        Set<Role> roleSet = roleService.list(qw).stream().collect(Collectors.toSet());
        return roleSet;
    }

    @Override
    public Set<Long> findRoleIdsByUserId(Long userId) {
        QueryWrapper<UserRole> qw = new QueryWrapper<>();
        qw.eq(AmosUtils.toDbField(UserRole::getUserId), userId);
        List<UserRole> roles = super.list(qw);
        Set<Long> resultRoles = roles.stream().map(UserRole::getRoleId).collect(Collectors.toSet());
        return resultRoles;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUserRole(Long userId, Set<Long> roleIds) {
        if (CollUtil.isNotEmpty(roleIds)) {
            //删除用户角色
            this.removeByUserIds(Collections.singleton(userId));
            //批量添加用户角色
            int res = baseMapper.addBatchRoles(userId, roleIds);
            return res > 0;
        }
        return Boolean.FALSE;
    }

    @Override
    public Boolean removeByUserIds(Set<Long> userIds) {
        UpdateWrapper<UserRole> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(UserRole::getUserId), userIds);
        return super.remove(uw);
    }
}
