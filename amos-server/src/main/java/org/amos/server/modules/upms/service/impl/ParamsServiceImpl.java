package org.amos.server.modules.upms.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.server.modules.upms.entity.Params;
import org.amos.server.modules.upms.mapper.ParamsMapper;
import org.amos.server.modules.upms.service.IParamsService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ParamsServiceImpl extends ServiceImpl<ParamsMapper, Params> implements IParamsService {

    @Override
    public Params findById(Long id) {
        return super.getById(id);
    }

    @Override
    @Log(value = "[更新参数配置]")
    public Boolean saveOrUpdateConfigParams(Params entity) {
        return super.saveOrUpdate(entity);
    }

    @Override
    @Log(value = "[删除参数配置]")
    public Boolean remove(Set<Long> ids) {
        UpdateWrapper<Params> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(Params::getId), ids).set(AmosUtils.toDbField(Params::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        return super.update(uw);
    }
}