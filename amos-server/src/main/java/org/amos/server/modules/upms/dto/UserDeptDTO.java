package org.amos.server.modules.upms.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDeptDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long userId;
    private Long deptId;
    private String deptName;
}
