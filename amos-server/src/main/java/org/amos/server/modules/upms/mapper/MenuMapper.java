/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.amos.server.modules.upms.entity.Menu;
import org.amos.server.modules.upms.vo.MenuVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统菜单 Mapper 接口
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> allMenu();

    /**
     * 自定义分页
     *
     * @param page
     * @param menu
     * @return
     */
    List<MenuVO> selectMenuPage(IPage page, MenuVO menu);

    /**
     * 获取对应角色菜单
     *
     * @param roles
     * @param type 0:按钮,1:菜单
     * @return
     */
    List<Menu> getMenusByRoles(@Param("roles") Set<Long> roles, @Param("type") Integer type);

    /**
     * 获取租户对应角色菜单
     *
     * @param tenantId 租户ID
     * @param alias 角色别名
     * @param type 0:按钮,1:菜单
     * @return
     */
    List<Menu> getMenusByTenant(@Param("tenantId") Long tenantId, @Param("alias") String alias, @Param("type") Integer type);

}
