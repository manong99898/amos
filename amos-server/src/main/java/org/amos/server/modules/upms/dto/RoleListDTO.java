package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.base.BaseDTO;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import java.io.Serializable;

/**
 * 角色列表DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class RoleListDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @MpQuery(column = "name", keyword = SqlKeyWordEnum.LIKE)
    private String name;

    @MpQuery(column = "remark", keyword = SqlKeyWordEnum.LIKE)
    private String remark;

}
