package org.amos.server.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.exception.ServiceException;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.server.modules.upms.entity.TenantPackage;
import org.amos.server.modules.upms.mapper.TenantPackageMapper;
import org.amos.server.modules.upms.service.ITenantPackageService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TenantPackageServiceImpl extends ServiceImpl<TenantPackageMapper, TenantPackage> implements ITenantPackageService {

    @Override
    public Boolean remove(Set<Long> ids) {
        // 系统默认ID 数据不能删除
        boolean contains = CollUtil.contains(ids, SystemConstant.SYS_DEFAULT_ID);
        if (contains) {
            throw new ServiceException("系统内置数据不能变更！");
        }

        UpdateWrapper<TenantPackage> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(TenantPackage::getId), ids)
        .set(AmosUtils.toDbField(TenantPackage::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        super.update(uw);
        return Boolean.TRUE;
    }

    @Override
    public TenantPackage selectById(Long packageId) {
        QueryWrapper<TenantPackage> qw = new QueryWrapper<>();
        qw.eq(AmosUtils.toDbField(TenantPackage::getId), packageId);
        qw.eq(AmosUtils.toDbField(TenantPackage::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_DEFAULT);
        return super.getOne(qw);
    }
}