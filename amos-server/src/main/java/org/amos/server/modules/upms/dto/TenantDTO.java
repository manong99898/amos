package org.amos.server.modules.upms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.amos.core.basic.base.BaseDTO;

import java.util.Date;

/**
 * 租户表
 */
@Data
public class TenantDTO extends BaseDTO {
	@ApiModelProperty(value = "租户编号")
	private String no;
	@ApiModelProperty(value = "租户名")
	private String name;
	@ApiModelProperty(value = "联系人的用户编号")
	private Long contactUserId;
	@ApiModelProperty(value = "联系人")
	private String contactName;
	@ApiModelProperty(value = "用户名称")
	private String username;
	@ApiModelProperty(value = "登录密码")
	private String password;
	@ApiModelProperty(value = "联系手机")
	private String contactMobile;
	@ApiModelProperty(value = "状态,0:禁用 1:启用")
	private Integer status;
	@ApiModelProperty(value = "绑定域名")
	private String domain;
	@ApiModelProperty(value = "租户套餐编号")
	private Long packageId;
	@ApiModelProperty(value = "过期时间")
	private Date expireTime;
	@ApiModelProperty(value = "账号数量")
	private Long accountCount;
}