/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.ParamsDTO;
import org.amos.server.modules.upms.entity.Params;
import org.amos.server.modules.upms.service.IParamsService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2021-01-04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/config/params")
public class ParamsController extends BaseController {

    private final IParamsService paramsService;

    @GetMapping("/list")
    @ApiOperation(value = "参数配置列表信息")
    @Log(value = "参数配置列表信息", logResultInfo = false)
    @SaCheckPermission("params:list")
    public R list(ParamsDTO dto) {
        IPage<Params> page = paramsService.page(initPage(dto), buildWrapper(dto));
        return R.ok(page);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新参数配置数据")
    @Log(value = "更新参数配置数据", logResultInfo = false)
    @SaCheckPermission("params:update")
    public R update(@RequestBody @Validated ParamsDTO dto) {
        Params params = AmosUtils.copy(dto, Params.class);
        paramsService.saveOrUpdateConfigParams(params);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除参数配置数据")
    @Log(value = "删除参数配置数据", logResultInfo = false)
    @SaCheckPermission("params:update")
    public R remove(@RequestBody Set<Long> ids) {
        paramsService.remove(ids);
        return R.ok();
    }
}
