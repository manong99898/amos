package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.amos.core.basic.base.BaseEntity;

/**
 * 参数配置表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_params")
public class Params extends BaseEntity {
    /**
     * 参数名称
     */
    private String name;
    /**
     * 参数键名
     */
    private String paramKey;
    /**
     * 参数键值
     */
    private String paramValue;
    /**
     * 状态，1:启用0:禁用
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
}