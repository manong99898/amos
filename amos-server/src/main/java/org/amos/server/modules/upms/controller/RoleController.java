/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.server.modules.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.annotation.Log;
import org.amos.core.basic.annotation.ParamCheck;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.server.modules.upms.dto.RoleDTO;
import org.amos.server.modules.upms.dto.RoleListDTO;
import org.amos.server.modules.upms.dto.RoleMenuDTO;
import org.amos.server.modules.upms.dto.RoleUserDTO;
import org.amos.server.modules.upms.entity.Role;
import org.amos.server.modules.upms.service.IRoleService;
import org.amos.server.modules.upms.service.IUserRoleService;
import org.amos.server.modules.upms.vo.RoleVO;
import org.amos.server.modules.upms.vo.UserRoleVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 角色表 前端控制器
 *
 * @author liubt
 * @since 2020-12-20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/role")
public class RoleController extends BaseController {

    private final IRoleService roleService;
    private final IUserRoleService userRoleService;

    @GetMapping("/list")
    @ApiOperation(value = "角色列表信息")
    @SaCheckPermission("role:list")
    @Log(value = "角色列表信息", logResultInfo = false)
    public R list(RoleListDTO dto) throws Exception {
        List<RoleVO> vos = roleService.selectList(dto);
        return R.ok(vos);
    }

    @PostMapping("/info")
    @ApiOperation(value = "用户角色信息")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("role:detail")
    @Log(value = "用户角色信息", logResultInfo = false)
    public R info(@RequestBody Set<Long> ids) {
        UserRoleVO vo = new UserRoleVO();
        Set<Long> roles = new HashSet<>();
        if (ids.size() == 1) {
            roles = userRoleService.findRoleIdsByUserId(ids.stream().findFirst().get());
        }
        vo.setIds(roles);
        return R.ok(vo);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新角色数据")
    @SaCheckPermission("role:update")
    @Log(value = "更新角色数据", logResultInfo = false)
    public R update(@RequestBody @Validated RoleDTO dto) {
        Role role = AmosUtils.copy(dto, Role.class);
        roleService.saveOrUpdateRole(role);
        return R.ok();
    }

    @PostMapping("/remove")
    @ApiOperation(value = "删除角色数据")
    @ParamCheck(message = "缺少必要参数")
    @SaCheckPermission("role:update")
    @SaCheckSafe
    @Log(value = "删除角色数据", logResultInfo = false)
    public R remove(@RequestBody Set<Long> ids) {
        roleService.remove(ids);
        return R.ok();
    }

    @PostMapping("/set/role-menu")
    @ApiOperation(value = "设置菜单角色")
    @SaCheckPermission("role:update")
    @SaCheckSafe
    @Log(value = "设置菜单角色", logResultInfo = false)
    public R setRoleMenu(@RequestBody @Validated RoleMenuDTO dto) {
        roleService.resetRolePermission(dto);
        return R.ok();
    }

    @PostMapping("/set/role-user")
    @ApiOperation(value = "设置用户角色")
    @SaCheckPermission("role:update")
    @SaCheckSafe
    @Log(value = "设置用户角色", logResultInfo = false)
    public R setRoleUser(@RequestBody @Validated RoleUserDTO dto) {
        roleService.resetRoleUser(dto);
        return R.ok();
    }
}
