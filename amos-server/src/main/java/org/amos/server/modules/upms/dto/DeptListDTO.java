package org.amos.server.modules.upms.dto;

import lombok.Data;
import org.amos.core.basic.annotation.MpQuery;
import org.amos.core.basic.enums.SqlKeyWordEnum;

import java.io.Serializable;

/**
 * 部门列表DTO
 *
 * @author CodeGenerator
 * @since 2020-12-20
 */
@Data
public class DeptListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @MpQuery(column = "name", keyword = SqlKeyWordEnum.LIKE)
    private String name;

}
