package org.amos.server.modules.upms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.amos.core.basic.base.BaseEntity;
import org.amos.core.frame.config.jackson.JsonStr2MapSerializer;

/**
 * 文件存储配置表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_config_file_storage", autoResultMap = true)
public class ConfigFileStorage extends BaseEntity {
    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 配置编码
     */
    private String no;
    /**
     * 配置名
     */
    private String name;
    /**
     * 存储平台
     */
    private Integer platform;
    /**
     * 存储配置
     */
    @JsonSerialize(using = JsonStr2MapSerializer.class)
    private String config;
    /**
     * 备注
     */
    private String remark;
    /**
     * 状态，1:启用0:禁用
     */
    private Integer status;
    /**
     * 是否为默认配置
     */
    private Integer isDefault;
}