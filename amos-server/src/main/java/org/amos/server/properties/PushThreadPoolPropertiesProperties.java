package org.amos.server.properties;

import lombok.Data;
import org.amos.core.frame.config.properties.AbstractThreadPoolProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc: 消息推送线程池配置
 * @author: liubt
 * @date: 2022-08-26 11:14
 **/
@Data
@Component
@ConfigurationProperties(prefix = "amos.task-pool.push")
public class PushThreadPoolPropertiesProperties extends AbstractThreadPoolProperties {
}

