package org.amos.server.listener;

import cn.dev33.satoken.listener.SaTokenListener;
import cn.dev33.satoken.stp.SaLoginModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @desc: 用户token监听器
 * @author: liubt
 * @date: 2022-08-23 14:36
 **/
@Slf4j
@Component
public class UserTokenListener implements SaTokenListener {

    /**
     * 每次登录时触发
     */
    @Override
    public void doLogin(String loginType, Object loginId, String tokenValue, SaLoginModel loginModel) {
        log.info("开始登录--------|loginType：{}，loginId：{},loginModel:{}", loginType, loginId, loginModel);
    }

    /**
     * 每次注销时触发
     */
    @Override
    public void doLogout(String loginType, Object loginId, String tokenValue) {
        log.info("开始注销登录--------|loginType：{}，loginId：{},tokenValue:{}", loginType, loginId, tokenValue);
    }

    /**
     * 每次被踢下线时触发
     */
    @Override
    public void doKickout(String loginType, Object loginId, String tokenValue) {
        log.info("踢下线--------|loginType：{}，loginId：{},tokenValue:{}", loginType, loginId, tokenValue);
    }

    /**
     * 每次被顶下线时触发
     */
    @Override
    public void doReplaced(String loginType, Object loginId, String tokenValue) {
        log.info("被顶下线--------|loginType：{}，loginId：{},tokenValue:{}", loginType, loginId, tokenValue);
    }

    /**
     * 每次被禁用时触发
     */
    @Override
    public void doDisable(String s, Object o, String s1, int i, long l) {

    }

    /**
     * 每次被解禁时触发
     */
    @Override
    public void doUntieDisable(String s, Object o, String s1) {

    }

    @Override
    public void doOpenSafe(String s, String s1, String s2, long l) {

    }

    @Override
    public void doCloseSafe(String s, String s1, String s2) {

    }

    /**
     * 每次创建Session时触发
     */
    @Override
    public void doCreateSession(String id) {
        // ...
    }

    /**
     * 每次注销Session时触发
     */
    @Override
    public void doLogoutSession(String id) {
        log.info("开始注销Session--------|id：{}", id);
    }

    /**
     * 每次更新过期时间时触发
     */
    @Override
    public void doRenewTimeout(String s, Object o, long l) {
        log.info("更新过期时间--------|s：{}，o：{},l:{}", s, o, l);
    }

}
