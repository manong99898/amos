package org.amos.server.constant;

/**
 * @author admin
 */
public interface SysCacheConstant {
    String SYS_CACHE_FORMAT = ":%s";
    /**
     * 用户根据id缓存Key
     */
    String SYS_USER_KEY = "sys:user:id";
    /**
     * 用户根据账号缓存Key
     */
    String SYS_USER_ACCOUNT_KEY = "sys:user:account";
    /**
     * 角色缓存Key
     */
    String SYS_ROLE_KEY = "sys:role";
    /**
     * 角色缓存Key
     */
    String SYS_USER_ROLE_KEY = "sys:user:role";
    /**
     * 字典缓存Key
     */
    String SYS_DICT_KEY = "sys:dict";
    /**
     * 字典详情缓存Key
     */
    String SYS_DICT_DETAIL_KEY = "sys:dict:detail";
    /**
     * 部门缓存Key
     */
    String SYS_DEPT_KEY = "sys:dept";
    /**
     * 短信配置缓存Key
     */
    String SYS_CONFIG_SMS_KEY = "sys:config:sms";
    /**
     * 邮件配置缓存Key
     */
    String SYS_CONFIG_MAIL_KEY = "sys:config:mail";
    /**
     * 参数配置缓存Key
     */
    String SYS_CONFIG_PARAMS_KEY = "sys:config:params";
    /**
     * 文件存储配置缓存Key
     */
    String SYS_CONFIG_FILE_STORAGE_KEY = "sys:config:file:storage";
    /**
     * 权限缓存Key
     */
    String SYS_PERMISSION_LIST_KEY = "sys:permission:list";
}
