package org.amos.satoken.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @desc: 用户状态枚举
 * @author: liubt
 * @date: 2022-08-18 16:48
 **/
@Getter
@AllArgsConstructor
public enum TokenStateEnums {
    NOT_TOKEN("-1", "未能读取到有效Token"),
    INVALID_TOKEN("-2", "Token无效"),
    TOKEN_TIMEOUT("-3", "Token已过期"),
    BE_REPLACED("-4", "Token已被顶下线"),
    KICK_OUT("-5", "Token已被踢下线");

    private String code;
    private String desc;

    public static String getDesc(String code) {
        for (TokenStateEnums state : TokenStateEnums.values()) {
            if (state.getCode().equals(code)) {
                return state.getDesc();
            }
        }
        return "当前会话未登录";
    }
}
