package org.amos.satoken.api;

import org.amos.satoken.domain.bo.AuthInfo;

import java.util.List;

/**
 * @desc: 用户认证接口
 * @author: liubt
 * @date: 2022-08-17 10:39
 **/
public interface UserAuthApi {
    /**
     * 根据用户名密码获取用户
     *
     * @param account
     * @return
     */
    AuthInfo getUserByAccount(String account);

    /**
     * 根据账户名和手机验证码称获取用户信息
     *
     * @param mobile
     * @return
     */
    AuthInfo getUserByMobile(String mobile);

    /**
     * 根据社交软件用户id和系统类别获取用户信息
     *
     * @param socialId
     * @param sourceType
     * @return
     */
    AuthInfo authBySocialIdAndSourceType(String socialId, Integer sourceType);

    /**
     * 根据userI的和loginType获取角色列表
     *
     * @param loginId
     * @param loginType
     * @return
     */
    List<String> getUserRoles(Object loginId, String loginType);

    /**
     * 根据userI的和loginType获取权限列表
     *
     * @param loginId
     * @param loginType
     * @return
     */
    List<String> getUserPermissions(Object loginId, String loginType);
}
