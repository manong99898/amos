package org.amos.satoken.handler;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.collection.CollUtil;
import lombok.RequiredArgsConstructor;
import org.amos.satoken.api.UserAuthApi;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @desc: 权限验证处理器, UserAuthApi强制实现
 * @author: liubt
 * @date: 2022-09-09 11:24
 **/
@Component
@RequiredArgsConstructor
public class PermissionValidHandler implements StpInterface {
    private final UserAuthApi userAuthApi;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<String> list = userAuthApi.getUserPermissions(loginId, loginType);
        if (CollUtil.isEmpty(list)) {
            return new ArrayList<>();
        }
        return list;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<String> list = userAuthApi.getUserRoles(loginId, loginType);
        if (CollUtil.isEmpty(list)) {
            return new ArrayList<>();
        }
        return list;
    }
}
