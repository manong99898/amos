package org.amos.satoken.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collections;
import java.util.List;

@Data
@ConfigurationProperties(prefix = AuthFilterProperties.PREFIX)
public class AuthFilterProperties {

    public static final String PREFIX = "amos.security.auth";

    /**
     * 白名单URL
     */
    private List<String> excludePaths = Collections.emptyList();
    /**
     * token 验证通过才能访问URL
     */
    private List<String> includePaths = Collections.emptyList();

}
