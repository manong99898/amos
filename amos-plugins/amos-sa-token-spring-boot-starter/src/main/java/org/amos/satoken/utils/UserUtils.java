package org.amos.satoken.utils;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import org.amos.satoken.domain.bo.AuthInfo;

/**
 * @desc: 用户信息工具类
 * @author: liubt
 * @date: 2022-08-19 09:18
 **/
public class UserUtils {
    /**
     * 获取当前用户信息
     *
     * @return
     */
    public static AuthInfo getUser() {
        try {
            AuthInfo authInfo = (AuthInfo) StpUtil.getSession().get(SaSession.USER);
            return authInfo;
        } catch (Exception e) {
            return null;
        }
    }
}
