package org.amos.satoken.domain.bo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class AuthInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @JSONField(serialize = false)
    private Long id;
    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 用户昵称
     */
    private String userName;
    /**
     * 手机号码
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 状态：0未激活、1启用、2锁定
     */
    @JSONField(serialize = false)
    private Integer status;
    /**
     * 是否是管理员：0否、1是
     */
    @JSONField(serialize = false)
    private Integer isAdmin;
    /**
     * 令牌
     */
    private String token;
    /**
     * token有效时间
     */
    private Long expire;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色别名
     */
    private Set<String> roleAlis;
    /**
     * 权限集合
     */
    private Set<String> permissions;
    /**
     * 密码
     */
    @JSONField(serialize = false)
    private String password;
}
