package org.amos.msg;

import lombok.extern.slf4j.Slf4j;
import org.amos.msg.model.EmailContentModel;
import org.amos.msg.params.PushMsgParam;
import org.amos.msg.platform.MsgPlatform;
import org.amos.msg.platform.email.EmailMsgConfig;
import org.amos.msg.platform.email.EmailMsgPlatform;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

@Slf4j
public class EmailMsgTest {
    @Test
    public void send() {
        EmailMsgConfig config = new EmailMsgConfig();
        config.setAuth(true);
        config.setSslEnable(false);
        config.setPass("xihglgzxdzfrbhdh");
        config.setHost("smtp.qq.com");
        config.setPort(25);
        config.setFrom("1731108137@qq.com");
        MsgPlatform platform = new EmailMsgPlatform("email", config);

        PushMsgParam param = new PushMsgParam();
        EmailContentModel model = new EmailContentModel();
        model.setTitle("邮件发送测试");
        model.setContent("hello word ");
        param.setReceiver(new HashSet<>(Arrays.asList("15295063375@163.com")));
        param.setContentModel(model);
        platform.push(param);
    }
}
