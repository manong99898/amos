package org.amos.msg;

import cn.hutool.core.lang.Dict;
import lombok.extern.slf4j.Slf4j;
import org.amos.core.basic.utils.JsonUtils;
import org.amos.msg.model.SmsContentModel;
import org.amos.msg.params.PushMsgParam;
import org.amos.msg.platform.sms.AliyunMsgConfig;
import org.amos.msg.platform.sms.AliyunMsgPlatform;
import org.amos.msg.platform.sms.TencentMsgConfig;
import org.amos.msg.platform.sms.TencentMsgPlatform;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

@Slf4j
public class SmsMsgTest {
    @Test
    public void aliyunSend() {
        AliyunMsgConfig config = new AliyunMsgConfig();
        config.setAccessKey("LTAI5tF6yGpGEWCXesdFewS");
        config.setSecretKey("I8E5DkzCaCMJirvfShh3NT1fKi12or");
        config.setSignature("阿里云短信测试");
        config.setTemplateCode("SMS_154950909");
        AliyunMsgPlatform platform = new AliyunMsgPlatform("aliyun", config);

        PushMsgParam param = new PushMsgParam();
        SmsContentModel model = new SmsContentModel();
        Dict of = Dict.of("code", "4321");
        model.setContent(JsonUtils.objectToJson(of));
        param.setReceiver(new HashSet<>(Arrays.asList("15299963333")));
        param.setContentModel(model);
        platform.push(param);
    }

    @Test
    public void tencentSend() {
        TencentMsgConfig config = new TencentMsgConfig();
        config.setAppId(123456789);
        config.setAppkey("CaC3MJi2rvfGhh1NT1fKi");
        config.setTemplateId(123456789);
        config.setSmsSign("amos_test");
        TencentMsgPlatform platform = new TencentMsgPlatform("tencent", config);

        PushMsgParam param = new PushMsgParam();
        SmsContentModel model = new SmsContentModel();
        Dict of = Dict.of("code", "4321");
        model.setContent(JsonUtils.objectToJson(of));
        param.setReceiver(new HashSet<>(Arrays.asList("15299963333")));
        param.setContentModel(model);
        platform.push(param);
    }
}
