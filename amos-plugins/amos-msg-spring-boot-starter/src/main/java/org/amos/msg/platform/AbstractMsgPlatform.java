package org.amos.msg.platform;

/**
 * 消息发送平台抽象类
 */
public abstract class AbstractMsgPlatform<T extends MsgPlatformConfig> implements MsgPlatform{

    /**
     * 配置唯一标识
     */
    private String id;
    /**
     * 客户端配置文件
     */
    protected T config;

    public AbstractMsgPlatform(String id, T config) {
        this.id = id;
        this.config = config;
        // 初始化客户端
        this.initClient();
    }

    public Boolean init(T config) {
        // 如果配置信息未变更则不更新
        if (config.equals(this.config)) {
            return Boolean.FALSE;
        }
        this.config = config;
        // 初始化客户端
        this.initClient();
        return Boolean.TRUE;
    }

    @Override
    public String getId() {
        return id;
    }

    /**
     * 初始化客户端
     */
    protected abstract void initClient();

}
