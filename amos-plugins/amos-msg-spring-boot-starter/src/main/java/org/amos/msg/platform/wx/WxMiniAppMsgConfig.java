package org.amos.msg.platform.wx;

import lombok.Data;
import org.amos.msg.platform.MsgPlatformConfig;

@Data
public class WxMiniAppMsgConfig implements MsgPlatformConfig {
    private String appId;
    private String appSecret;
}
