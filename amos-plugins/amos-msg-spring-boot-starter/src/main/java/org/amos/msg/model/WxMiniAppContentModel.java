package org.amos.msg.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author LBT
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WxMiniAppContentModel extends ContentModel {
    /**
     * 模板Id
     */
    private String templateId;
    /**
     * 模板消息发送的数据
     */
    Map<String, String> miniProgramParam;

    /**
     * 跳转链接
     */
    private String page;

}
