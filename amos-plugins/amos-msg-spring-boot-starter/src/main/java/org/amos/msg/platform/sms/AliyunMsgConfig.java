package org.amos.msg.platform.sms;

import lombok.Data;
import org.amos.msg.platform.MsgPlatformConfig;

@Data
public class AliyunMsgConfig implements MsgPlatformConfig {
    /**
     * 阿里短信服务accessKey
     */
    private String accessKey;

    /**
     * 阿里短信服务accesskeySecret
     */
    private String secretKey;

    /**
     * 短信模板code
     */
    private String templateCode;

    /**
     * 短信签名
     */
    private String signature;
}
