package org.amos.msg;

import lombok.extern.slf4j.Slf4j;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.msg.platform.MsgPlatformManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ComponentScan(basePackages = {SystemConstant.SYS_PACKAGE + ".msg*"})
public class MsgAutoConfiguration {

    @Bean
    public MsgPlatformManager msgPlatformManager() {
        return new MsgPlatformManager();
    }
}
