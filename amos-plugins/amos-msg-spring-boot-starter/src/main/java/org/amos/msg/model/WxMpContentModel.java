package org.amos.msg.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author LBT
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WxMpContentModel extends ContentModel {
    /**
     * appId
     */
    private String appId;
    /**
     * 模板Id
     */
    private String templateId;

    /**
     * 模板消息跳转小程序的appid
     */
    private String miniProgramId;

    /**
     * 模板消息发送的数据
     */
    private Map<String, String> WxMpParam;

    /**
     * 模板消息发送的数据对应字体颜色
     */
    private Map<String, String> WxMpParamColor;

    /**
     * 模板消息跳转的url
     */
    private String url;

    /**
     * 模板消息跳转小程序的页面路径
     */
    private String path;

}
