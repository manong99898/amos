package org.amos.msg.platform.wx;

import lombok.Data;
import org.amos.msg.platform.MsgPlatformConfig;

@Data
public class WxMpMsgConfig implements MsgPlatformConfig {
    /**
     * 微信公众号的appid
     */
    private String appId;

    /**
     * 微信公众号的app secret
     */
    private String secret;

    /**
     * 微信公众号的token
     */
    private String token;

    /**
     * 微信公众号的EncodingAESKey
     */
    private String aesKey;
}
