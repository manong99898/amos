package org.amos.msg.platform.app;

import org.amos.msg.params.PushMsgParam;
import org.amos.msg.platform.AbstractMsgPlatform;

public class AppMsgPlatform extends AbstractMsgPlatform<AppMsgConfig> {
    public AppMsgPlatform(String id, AppMsgConfig config) {
        super(id, config);
    }

    @Override
    protected void initClient() {

    }

    @Override
    public Boolean push(PushMsgParam param) {
        return null;
    }
}
