package org.amos.msg.platform;

import org.amos.msg.params.PushMsgParam;

public interface MsgPlatform {
    /**
     * 配置唯一标识
     */
    String getId();

    /**
     * 推送消息
     * @param param
     * @return
     */
    Boolean push(PushMsgParam param);
}
