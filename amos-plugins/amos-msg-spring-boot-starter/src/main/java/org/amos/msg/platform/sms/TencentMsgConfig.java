package org.amos.msg.platform.sms;

import lombok.Data;
import org.amos.msg.platform.MsgPlatformConfig;

@Data
public class TencentMsgConfig implements MsgPlatformConfig {
    /**
    * 短信应用SDK AppID.
    */
    private int appId;

    /**
     * 短信应用SDK AppKey.
     */
    private String appkey;

    /**
     * 短信签名.
     */
    private String smsSign;
    /**
     * 模版ID
     */
    private Integer templateId;
}
