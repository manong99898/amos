package org.amos.msg.enums;

import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum MsgTypeEnum {
    SMS("SMS",  0),
    EMAIL("EMAIL",  1),
    WX("WX", 2);

    /**
     * 消息平台标识
     */
    private final String code;
    /**
     * 消息平台字典编码
     */
    private final Integer type;


    public static MsgTypeEnum getByPlatform(String code) {
        return ArrayUtil.firstMatch(o -> o.getCode().equals(code), values());
    }

    public static MsgTypeEnum getByPlatform(Integer code) {
        return ArrayUtil.firstMatch(o -> o.getCode().equals(code), values());
    }
}
