package org.amos.msg.params;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.amos.msg.model.ContentModel;

import java.util.Set;

/**
 * 发送任务信息
 *
 * @author LBT
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PushMsgParam {

    /**
     * 业务Id
     */
    private String businessId;

    /**
     * 接收者
     */
    private Set<String> receiver;

    /**
     * 发送文案模型
     */
    private ContentModel contentModel;
}
