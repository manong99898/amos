package org.amos.msg.platform.email;

import lombok.Data;
import org.amos.msg.platform.MsgPlatformConfig;

@Data
public class EmailMsgConfig implements MsgPlatformConfig {
    /**
     * 邮件服务器的SMTP地址，可选，默认为smtp.<发件人邮箱后缀>
     */
    private String host;
    /**
     * 邮件服务器的SMTP端口，可选，默认25
     */
    private Integer port;
    /**
     * 用户名，默认为发件人邮箱前缀
     */
    private String user;
    /**
     * 密码
     */
    private String pass;
    /**
     * 发件人（必须正确，否则发送失败）
     */
    private String from;
    /**
     * 使用 STARTTLS安全连接，STARTTLS是对纯文本通信协议的扩展。
     */
    private String starttlsEnable;
    /**
     * 是否开启授权校验
     */
    private Boolean auth;
    /**
     * 使用SSL安全连接
     */
    private Boolean sslEnable;
    /**
     * SMTP超时时长，单位毫秒，缺省值不超时
     */
    private long timeout;
    /**
     * Socket连接超时值，单位毫秒，缺省值不超时
     */
    private long connectionTimeout;
}
