package org.amos.msg.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 发送内容的模型
 * (不同的渠道会有不同的消息体)
 *
 * @author LBT
 */
@Data
public class ContentModel implements Serializable {

    /**
     * 发送文案
     */
    private String content;

    /**
     * 短信类型，0验证码
     */
    private Integer type;
}
