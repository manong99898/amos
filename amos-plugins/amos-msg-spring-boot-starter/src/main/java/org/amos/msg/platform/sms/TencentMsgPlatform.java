package org.amos.msg.platform.sms;

import com.github.qcloudsms.SmsMultiSender;
import com.github.qcloudsms.SmsMultiSenderResult;
import lombok.extern.slf4j.Slf4j;
import org.amos.core.basic.utils.JsonUtils;
import org.amos.msg.model.SmsContentModel;
import org.amos.msg.params.PushMsgParam;
import org.amos.msg.platform.AbstractMsgPlatform;

import java.util.ArrayList;

@Slf4j
public class TencentMsgPlatform extends AbstractMsgPlatform<TencentMsgConfig> {
    private static final String DEFAULT_NATION_CODE = "86";

    private SmsMultiSender client;
    public TencentMsgPlatform(String id, TencentMsgConfig config) {
        super(id, config);
    }

    @Override
    protected void initClient() {
        client = new SmsMultiSender(config.getAppId(), config.getAppkey());
    }

    @Override
    public Boolean push(PushMsgParam param) {
        try {
            int templateId = config.getTemplateId();
            String smsSign = config.getSmsSign();
            SmsContentModel contentModel = (SmsContentModel) param.getContentModel();
            ArrayList<String> phones = new ArrayList<>(param.getReceiver());
            ArrayList<String> params = JsonUtils.jsonToPojo(contentModel.getContent(), ArrayList.class);
            SmsMultiSenderResult result = client.sendWithParam(DEFAULT_NATION_CODE, phones, templateId, params, smsSign, "", "");

            if (result.result == 0) {
                return Boolean.TRUE;
            } else {
                log.error("[腾讯云短信发送]，请求发送错误：{}", JsonUtils.objectToJson(result));
            }
        } catch (Exception e) {
            log.error("[腾讯云短信发送]，异常信息：{}", JsonUtils.objectToJson(e));
        }
        return Boolean.FALSE;
    }
}
