package org.amos.msg.enums;

import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.amos.msg.platform.AbstractMsgPlatform;
import org.amos.msg.platform.MsgPlatformConfig;
import org.amos.msg.platform.email.EmailMsgConfig;
import org.amos.msg.platform.email.EmailMsgPlatform;
import org.amos.msg.platform.sms.AliyunMsgConfig;
import org.amos.msg.platform.sms.AliyunMsgPlatform;
import org.amos.msg.platform.sms.TencentMsgConfig;
import org.amos.msg.platform.sms.TencentMsgPlatform;
import org.amos.msg.platform.wx.WxMiniAppMsgConfig;
import org.amos.msg.platform.wx.WxMiniAppMsgPlatform;
import org.amos.msg.platform.wx.WxMpMsgConfig;
import org.amos.msg.platform.wx.WxMpMsgPlatform;


@Getter
@AllArgsConstructor
public enum MsgPlatformEnum {
    ALI_SMS("ALI_SMS", 0, 0,AliyunMsgConfig.class, AliyunMsgPlatform.class),
    TX_SMS("TX_SMS", 1, 0, TencentMsgConfig.class, TencentMsgPlatform.class),
    EMAIL("EMAIL", 0, 1, EmailMsgConfig.class, EmailMsgPlatform.class),
    WX_MP("WX_MP", 0, 2, WxMiniAppMsgConfig.class, WxMiniAppMsgPlatform.class),
    WX_MINI("WX_MINI", 1, 2, WxMpMsgConfig.class, WxMpMsgPlatform.class);

    /**
     * 消息平台标识
     */
    private final String code;
    /**
     * 消息平台字典编码
     */
    private final Integer platform;
    /**
     * 消息平台类型
     */
    private final Integer platformType;
    /**
     * 配置类
     */
    private final Class<? extends MsgPlatformConfig> configClass;
    /**
     * 客户端类
     */
    private final Class<? extends AbstractMsgPlatform> clientClass;

    public static MsgPlatformEnum getByPlatform(Integer platform, Integer platformType) {
        return ArrayUtil.firstMatch(o -> o.getPlatform().equals(platform) && o.getPlatformType().equals(platformType), values());
    }
    public static MsgPlatformEnum getByPlatform(String platformCode) {
        return ArrayUtil.firstMatch(o -> o.getCode().equals(platformCode), values());
    }
}
