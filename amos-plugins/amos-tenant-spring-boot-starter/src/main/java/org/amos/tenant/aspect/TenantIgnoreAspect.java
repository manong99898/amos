package org.amos.tenant.aspect;

import lombok.extern.slf4j.Slf4j;
import org.amos.tenant.annotation.TenantIgnore;
import org.amos.tenant.core.TenantContextHolder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * 忽略多租户切面
 */
@Aspect
@Slf4j
public class TenantIgnoreAspect {

    @Around("@annotation(org.amos.tenant.annotation.TenantIgnore)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        TenantIgnore annotation = methodSignature.getMethod().getAnnotation(TenantIgnore.class);
        Boolean ignore = TenantContextHolder.isIgnore();
        try {
            TenantContextHolder.setIgnore(annotation.enabled());
            // 执行逻辑
            return joinPoint.proceed();
        } finally {
            TenantContextHolder.setIgnore(ignore);
        }
    }

}
