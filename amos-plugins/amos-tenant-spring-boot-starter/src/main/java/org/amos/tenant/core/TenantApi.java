package org.amos.tenant.core;

/**
 * 多租户业务相关api
 */
public interface TenantApi {
    /**
     * 根据id 校验租户
     * @param tenantId
     */
    void valid(Long tenantId);

    /**
     * 根据租户编号获取租户ID
     * @param tenantNo 租户编号
     * @return 租户ID
     */
    Long getTenantId(String tenantNo);
}
