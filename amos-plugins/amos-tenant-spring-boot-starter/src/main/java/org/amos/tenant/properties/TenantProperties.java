package org.amos.tenant.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collections;
import java.util.List;

/**
 * 多租户配置
 */
@Data
@ConfigurationProperties(prefix = TenantProperties.PREFIX)
public class TenantProperties {
    public static final String PREFIX = "amos.tenant";

    /**
     * 是否开启多租户
     */
    private Boolean enabled = true;

    /**
     * 白名单URL
     *
     */
    private List<String> excludePaths = Collections.emptyList();

    /**
     * 白名单数据表
     *
     */
    private List<String> excludeTables = Collections.emptyList();

}
