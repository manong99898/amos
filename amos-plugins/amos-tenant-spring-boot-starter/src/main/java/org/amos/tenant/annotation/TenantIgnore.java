package org.amos.tenant.annotation;

import java.lang.annotation.*;

/**
 * 租户白名单，标记指定方法是否进行租户过滤
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface TenantIgnore {
    /**
     * 是否开启过滤租户，默认开启
     */
    boolean enabled() default true;
}
