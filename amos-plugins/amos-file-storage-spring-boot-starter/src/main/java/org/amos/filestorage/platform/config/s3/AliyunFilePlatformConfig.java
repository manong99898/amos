package org.amos.filestorage.platform.config.s3;

import lombok.Data;
import org.amos.filestorage.platform.FilePlatformConfig;

@Data
public class AliyunFilePlatformConfig extends CommonS3FilePlatFromConfig implements FilePlatformConfig {
    private String endPoint;

}
