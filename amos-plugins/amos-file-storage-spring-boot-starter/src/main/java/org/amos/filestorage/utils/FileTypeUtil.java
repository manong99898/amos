package org.amos.filestorage.utils;

import org.amos.filestorage.exception.FileStorageException;
import org.apache.tika.Tika;

import java.io.File;
import java.io.IOException;

public class FileTypeUtil {
    private static final Tika tika = new Tika();

    /**
     * 通过文件名，获取文件类型
     *
     * @param name 文件名
     * @return
     */
    public static String getType(String name) {
        return tika.detect(name);
    }

    /**
     * 通过文件获取文件类型
     *
     * @param file 文件内容
     * @return
     */
    public static String getType(File file) {
        try {
            return tika.detect(file);
        } catch (IOException e) {
            throw new FileStorageException(e);
        }
    }

    /**
     * 通过文件内容获取文件类型
     *
     * @param data 文件内容
     * @return
     */
    public static String getType(byte[] data) {
        return tika.detect(data);
    }

    /**
     * 通过文件名、文件内容获取文件类型
     *
     * @param data 文件内容
     * @param name 文件名
     * @return
     */
    public static String getType(byte[] data, String name) {
        return tika.detect(data, name);
    }
}
