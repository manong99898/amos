package org.amos.filestorage.platform.config.s3;

import lombok.Data;

@Data
public class CommonS3FilePlatFromConfig {
    private String accessKey;
    private String secretKey;
    private String bucket;
    private String domain;
}
