package org.amos.filestorage.platform.config;

import lombok.Data;
import org.amos.filestorage.platform.FilePlatformConfig;

@Data
public class FtpFilePlatformConfig implements FilePlatformConfig {
    /**
     * 主机
     */
    private String host;
    /**
     * 端口，默认21
     */
    private int port;
    /**
     * 用户名，默认 anonymous（匿名）
     */
    private String user;
    /**
     * 密码，默认空
     */
    private String password;
    /**
     * 编码，默认UTF-8
     */
    private String charset;
    /**
     * 连接超时时长，单位毫秒，默认10秒
     */
    private long connectionTimeout = 10000;
    /**
     * Socket连接超时时长，单位毫秒，默认10秒
     */
    private long soTimeout = 10000;
    /**
     * 服务器语言，默认空
     */
    private String serverLanguageCode;
    /**
     * 服务器标识，默认空
     *
     */
    private String systemKey;
    /**
     * 连接模式，默认被动模式
     */
    private String mode;
    /**
     * 访问域名
     */
    private String domain;
    /**
     * 基础路径
     */
    private String basePath;
}
