package org.amos.filestorage;

import lombok.extern.slf4j.Slf4j;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.filestorage.platform.FilePlatformManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ComponentScan(basePackages = {SystemConstant.SYS_PACKAGE + ".filestorage*"})
public class FileStorageAutoConfiguration {

    @Bean
    public FilePlatformManager filePlatformManager() {
        return new FilePlatformManager();
    }
}
