package org.amos.filestorage.platform.config;

import lombok.Data;
import org.amos.filestorage.platform.FilePlatformConfig;

@Data
public class LocalFilePlatformConfig implements FilePlatformConfig {
    /**
     * 基础路径
     */
    private String basePath;
    /**
     * 访问域名
     */
    private String domain;
}
