package org.amos.filestorage.platform.config;

import lombok.Data;
import org.amos.filestorage.platform.FilePlatformConfig;

@Data
public class SftpFilePlatformConfig implements FilePlatformConfig {
    /**
     * 主机
     */
    private String host;
    /**
     * 端口，默认21
     */
    private int port;
    /**
     * 用户名
     */
    private String user;
    /**
     * 密码，默认空
     */
    private String password;
    /**
     * 私钥路径，默认空
     */
    private String privateKeyPath;
    /**
     * 编码，默认UTF-8
     */
    private String charset;
    /**
     * 连接超时时长，单位毫秒，默认10秒
     */
    private long connectionTimeout;
    private String domain;
    /**
     * 基础路径
     */
    private String basePath;
}
