package org.amos.filestorage.platform;

import java.io.InputStream;
import java.util.function.Consumer;

/**
 * 文件存储平台
 */
public interface FilePlatform {
    /**
     * 配置唯一标识
     */
    String getId();
    /**
     * 上传文件
     * @param bytes 文件流
     * @param path  相对路径
     * @param type  文件类型
     * @return 文件访问路径
     */
    String upload(byte[] bytes, String path, String type);
    /**
     * 删除文件
     */
    Boolean delete(String path);

    /**
     * 文件是否存在
     */
    Boolean exists(String path);
    /**
     * 下载文件
     */
    void download(String path, Consumer<InputStream> consumer);
}
