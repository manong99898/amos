package org.amos.filestorage.enums;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.amos.filestorage.platform.AbstractFilePlatform;
import org.amos.filestorage.platform.FilePlatformConfig;
import org.amos.filestorage.platform.config.FtpFilePlatformConfig;
import org.amos.filestorage.platform.config.LocalFilePlatformConfig;
import org.amos.filestorage.platform.config.SftpFilePlatformConfig;
import org.amos.filestorage.platform.config.s3.*;
import org.amos.filestorage.platform.ftp.FtpFilePlatform;
import org.amos.filestorage.platform.local.LocalFilePlatform;
import org.amos.filestorage.platform.s3.*;
import org.amos.filestorage.platform.ftp.SftpFilePlatform;

import java.util.*;

/**
 * 文件存储平台枚举类
 */
@AllArgsConstructor
@Getter
public enum FilePlatformEnum {


    LOCAL("LOCAL", 0,"本地存储", LocalFilePlatformConfig.class, LocalFilePlatform.class),
    FTP("FTP", 1,"FTP存储", FtpFilePlatformConfig.class, FtpFilePlatform.class),
    SFTP("SFTP", 2,"SFTP存储", SftpFilePlatformConfig.class, SftpFilePlatform.class),
    MINI_IO("MINI_IO", 3,"MinIO", MinIOFilePlatformConfig.class, MinIOFilePlatform.class),
    ALIYUN("ALIYUN", 4,"阿里云", AliyunFilePlatformConfig.class, AliyunFilePlatform.class),
    TENCENT("TENCENT", 5,"腾讯云", TencentFilePlatformConfig.class, TencentFilePlatform.class),
    QINIU("QINIU", 6,"七牛云", QiniuFilePlatformConfig.class, QiniuFilePlatform.class),
    UPYUN("UPYUN", 7,"又拍云", UpyunFilePlatformConfig.class, UpyunFilePlatform.class);

    /**
     * 存储平台标识
     */
    private final String platform;
    /**
     * 存储平台字典编码
     */
    private final Integer code;

    /**
     * 存储平台名称
     */
    private final String name;

    /**
     * 配置类
     */
    private final Class<? extends FilePlatformConfig> configClass;
    /**
     * 客户端类
     */
    private final Class<? extends AbstractFilePlatform> platformClass;

    public static FilePlatformEnum getByPlatform(String platform) {
        return ArrayUtil.firstMatch(o -> o.getPlatform().equals(platform), values());
    }

    public static FilePlatformEnum getByPlatform(Integer platformCode) {
        return ArrayUtil.firstMatch(o -> o.getCode().equals(platformCode), values());
    }

    public static String getByPlatformName(String platform) {
        FilePlatformEnum platformEnum = ArrayUtil.firstMatch(o -> o.getPlatform().equals(platform), values());
        if (Objects.nonNull(platformEnum)) {
            return platformEnum.getName();
        }
        return "未知平台";
    }

    public static String getByPlatformByCode(Integer platformCode) {
        FilePlatformEnum platformEnum = ArrayUtil.firstMatch(o -> o.getCode().equals(platformCode), values());
        if (Objects.nonNull(platformEnum)) {
            return platformEnum.getPlatform();
        }
        return "未知平台";
    }

    public static List<Map<String, Object>> getPlatformList() {
        List<Map<String, Object>> maps = new ArrayList<>();
        List<FilePlatformEnum> enumList = Arrays.asList(values());
        enumList.forEach(x -> {
            maps.add(Dict.of("id", x.getPlatform(), "name", x.getName()));
        });
        return maps;
    }
}
