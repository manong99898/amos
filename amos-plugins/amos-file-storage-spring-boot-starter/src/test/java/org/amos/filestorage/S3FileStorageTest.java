package org.amos.filestorage;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.amos.filestorage.platform.config.s3.*;
import org.amos.filestorage.platform.s3.*;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.Date;
@Slf4j
public class S3FileStorageTest {

    @Test
    public void uploadMinIO() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        MinIOFilePlatformConfig config = new MinIOFilePlatformConfig();
        config.setEndPoint("http://101.43.21.212:19000");
        config.setBucket("public");
        config.setAccessKey("root");
        config.setSecretKey("root@123");
        MinIOFilePlatform platform = new MinIOFilePlatform("minio", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test9.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("S3文件上传，文件访问路径:{}", uploadPath);
    }

    @Test
    public void uploadTencent() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        TencentFilePlatformConfig config = new TencentFilePlatformConfig();
        config.setRegion("ap-nanjing");
        config.setDomain("https://amos-1258660306.cos.ap-nanjing.myqcloud.com");
        config.setBucket("amos-1258660306");
        config.setAccessKey("AKIDfAUcaaLsFRQCl8QcigYEiA62rXGpDg3G");
        config.setSecretKey("7BAzd5QkxGXo2wmQvHUceWCmO5hDZn2P");
        TencentFilePlatform platform = new TencentFilePlatform("tencent", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test5.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("S3文件上传，文件访问路径:{}", uploadPath);
    }

    @Test
    public void uploadAliyun() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        AliyunFilePlatformConfig config = new AliyunFilePlatformConfig();
        config.setDomain("https://amos-public.oss-cn-beijing.aliyuncs.com");
        config.setBucket("amos-public");
        config.setEndPoint("oss-cn-beijing.aliyuncs.com");
        config.setAccessKey("LTAI5tF6yGpGEWCXeAqhouAH");
        config.setSecretKey("I8E5DkzCaCMJirvfGhh1NT1fKi12or");
        AliyunFilePlatform platform = new AliyunFilePlatform("aliyun", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test5.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("S3文件上传，文件访问路径:{}", uploadPath);
    }

    @Test
    public void uploadQiniuyun() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        QiniuFilePlatformConfig config = new QiniuFilePlatformConfig();
        config.setDomain("http://rwco8122i.sabkt.gdipper.com");
        config.setRegion("xinjiapo");
        config.setBucket("amos-xjp");
        config.setAccessKey("e1g6nc4Ki6__pJskIg9EHH7Wdh84iHgspE8giW1J");
        config.setSecretKey("ezmeLsQuB2Ed2joslTiXj1LsaGpydET85b5_YT2c");
        QiniuFilePlatform platform = new QiniuFilePlatform("qiniuyun", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test5.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("S3文件上传，文件访问路径:{}", uploadPath);
    }

    @Test
    public void uploadUpyun() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        UpyunFilePlatformConfig config = new UpyunFilePlatformConfig();
        config.setDomain("http://amos-test.test.upcdn.net");
        config.setBucket("amos-test");
        config.setAccessKey("amostest");
        config.setSecretKey("oKdZbBRrjLNhUVxtroBF9kCvAxtowiOE");
        UpyunFilePlatform platform = new UpyunFilePlatform("upyun", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test5.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("S3文件上传，文件访问路径:{}", uploadPath);
    }
}
