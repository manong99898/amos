package org.amos.filestorage;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.amos.filestorage.platform.config.LocalFilePlatformConfig;
import org.amos.filestorage.platform.local.LocalFilePlatform;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.Date;

@Slf4j
public class LocalFileStorageTest {
    @Test
    public void upload() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        LocalFilePlatformConfig config = new LocalFilePlatformConfig();
        config.setBasePath("D:/file/upload_test/");
        config.setDomain("http://127.0.0.1:8088");
        LocalFilePlatform platform = new LocalFilePlatform("local", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test4.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("本地文件上传，文件访问路径:{}", uploadPath);
    }

    @Test
    public void delete() {

    }

}
