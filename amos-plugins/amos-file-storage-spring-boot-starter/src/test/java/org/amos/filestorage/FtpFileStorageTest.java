package org.amos.filestorage;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.amos.filestorage.platform.config.FtpFilePlatformConfig;
import org.amos.filestorage.platform.ftp.FtpFilePlatform;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.Date;

@Slf4j
public class FtpFileStorageTest {
    @Test
    public void uploadFtp() {
        byte[] bytes = FileUtil.readBytes("D:/file/test/test1.json");
        FtpFilePlatformConfig config = new FtpFilePlatformConfig();
        config.setHost("192.168.31.182");
        config.setPort(21);
        config.setUser("amos");
        config.setPassword("123456");
        config.setCharset("UTF-8");
        config.setMode("Passive");
        FtpFilePlatform platform = new FtpFilePlatform("ftp", config);
        String path = DateUtil.format(new Date(), "yyyy/MM/dd") + "/"+ "test9.json";
        String uploadPath = platform.upload(bytes, path, MediaType.APPLICATION_JSON_VALUE);
        log.info("S3文件上传，文件访问路径:{}", uploadPath);
    }
}
