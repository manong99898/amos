package org.amos.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.amos.gen.entity.GenColumnInfo;

/**
 * 代码生成字段信息配置 Mapper 接口
 *
 * @author CodeGenerator
 * @since 2020-12-18
 */
public interface GenColumnMapper extends BaseMapper<GenColumnInfo> {

}
