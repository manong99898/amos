package org.amos.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.amos.gen.entity.GenTableInfo;

/**
 * 代码生成表信息配置 Mapper 接口
 *
 * @author CodeGenerator
 * @since 2020-12-18
 */
public interface GenTableMapper extends BaseMapper<GenTableInfo> {
    /**
     * 查询代码生成配置信息
     *
     * @param tableName
     * @return
     */
    GenTableInfo selectGenInfo(String tableName);
}
