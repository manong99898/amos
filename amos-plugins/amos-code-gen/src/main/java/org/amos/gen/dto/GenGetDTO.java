package org.amos.gen.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class GenGetDTO {
    @NotBlank(message = "表名不能为空")
    private String tableName;
}
