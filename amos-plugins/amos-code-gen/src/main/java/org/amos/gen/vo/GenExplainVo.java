package org.amos.gen.vo;

import lombok.Data;
import org.amos.core.basic.node.TreeNode;
import org.amos.gen.entity.GenTemplateExplain;

import java.util.List;
import java.util.Objects;

/**
 * @desc: 表达式VO
 * @author: liubt
 * @date: 2022-11-03 13:49
 **/
@Data
public class GenExplainVo extends GenTemplateExplain implements TreeNode<Long, GenExplainVo> {

    private List<GenExplainVo> children;

    @Override
    public Long id() {
        return super.getId();
    }

    @Override
    public Long parentId() {
        return super.getParentId();
    }

    @Override
    public boolean root() {
        return Objects.equals(this.getParentId(), 0L);
    }

    @Override
    public void setChildren(List<GenExplainVo> children) {
        this.children = children;
    }
}
