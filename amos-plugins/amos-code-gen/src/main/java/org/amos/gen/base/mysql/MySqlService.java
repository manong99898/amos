package org.amos.gen.base.mysql;

import org.amos.gen.base.GeneratorConfig;
import org.amos.gen.base.SQLService;
import org.amos.gen.base.TableSelector;

public class MySqlService implements SQLService {

    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new MySqlTableSelector(new MySqlColumnSelector(generatorConfig), generatorConfig);
    }

}
