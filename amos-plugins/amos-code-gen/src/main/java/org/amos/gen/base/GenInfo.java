package org.amos.gen.base;

import lombok.Data;

import java.util.List;

/**
 * @desc: 代码生成信息
 * @author: liubt
 * @date: 2022-10-11 11:01
 **/
@Data
public class GenInfo {

    private Long datasourceId;

    private List<String> tableNames;

    private List<Long> templateIds;

    private String author;

    private String packageName;

    private String tablePrefix;

    private String charset = "UTF-8";
}
