package org.amos.gen.base.dm;

import org.amos.gen.base.GeneratorConfig;
import org.amos.gen.base.SQLService;
import org.amos.gen.base.TableSelector;

public class DmService implements SQLService {

    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new DmTableSelector(new DmColumnSelector(generatorConfig), generatorConfig);
    }

}
