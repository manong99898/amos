/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.gen.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.gen.dto.GenTemplateExplainDTO;
import org.amos.gen.entity.GenTemplateExplain;
import org.amos.gen.service.GenTemplateExplainService;
import org.amos.gen.vo.GenExplainVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 *  模版表达式说明 前端控制器
 *
 * @author CodeGenerator
 * @since 2021-01-19
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/gen/template-explain")
public class GenTemplateExplainController extends BaseController {

    private final GenTemplateExplainService genTemplateExplainService;

    @GetMapping("/list")
    public R list(GenTemplateExplainDTO dto) {
        List<GenExplainVo> list = genTemplateExplainService.selectTree(buildWrapper(dto));
        return R.ok(list);
    }

    @PostMapping("/update")
    public R update(@RequestBody @Validated GenTemplateExplainDTO dto) {
        GenTemplateExplain explain = AmosUtils.copy(dto, GenTemplateExplain.class);
        genTemplateExplainService.saveOrUpdate(explain);
        return R.ok();
    }

    @PostMapping("/remove")
    public R remove(@RequestBody Set<Long> ids) {
        UpdateWrapper<GenTemplateExplain> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(GenTemplateExplain::getId), ids)
                .set(AmosUtils.toDbField(GenTemplateExplain::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        genTemplateExplainService.update(uw);
        return R.ok();
    }
}
