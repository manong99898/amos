package org.amos.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.amos.gen.entity.GenTableInfo;

/**
 * 代码生成表信息配置 服务类
 *
 * @author CodeGenerator
 * @since 2020-12-18
 */
public interface GenTableService extends IService<GenTableInfo> {

}
