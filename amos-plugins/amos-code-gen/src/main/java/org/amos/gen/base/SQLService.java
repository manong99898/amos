package org.amos.gen.base;


public interface SQLService {

    TableSelector getTableSelector(GeneratorConfig generatorConfig);

}
