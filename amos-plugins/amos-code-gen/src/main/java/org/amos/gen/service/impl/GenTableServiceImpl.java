package org.amos.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.gen.entity.GenTableInfo;
import org.amos.gen.mapper.GenTableMapper;
import org.amos.gen.service.GenTableService;
import org.springframework.stereotype.Service;

/**
 * 代码生成表信息配置 服务实现类
 *
 * @author CodeGenerator
 * @since 2020-12-18
 */
@Service
public class GenTableServiceImpl extends ServiceImpl<GenTableMapper, GenTableInfo> implements GenTableService {

}
