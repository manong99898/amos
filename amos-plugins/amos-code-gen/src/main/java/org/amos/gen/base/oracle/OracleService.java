package org.amos.gen.base.oracle;

import org.amos.gen.base.GeneratorConfig;
import org.amos.gen.base.SQLService;
import org.amos.gen.base.TableSelector;

public class OracleService implements SQLService {

    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new OracleTableSelector(new OracleColumnSelector(generatorConfig), generatorConfig);
    }

}
