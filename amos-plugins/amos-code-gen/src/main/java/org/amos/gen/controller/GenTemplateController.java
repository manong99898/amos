/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.gen.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.RequiredArgsConstructor;
import org.amos.core.basic.base.BaseController;
import org.amos.core.basic.constant.SystemConstant;
import org.amos.core.basic.utils.AmosUtils;
import org.amos.core.basic.vo.R;
import org.amos.gen.dto.GenTemplateDTO;
import org.amos.gen.dto.GenTemplateGroupDTO;
import org.amos.gen.entity.GenTemplate;
import org.amos.gen.entity.GenTemplateGroup;
import org.amos.gen.service.GenTemplateGroupService;
import org.amos.gen.service.GenTemplateService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 模板管理 前端控制器
 *
 * @author CodeGenerator
 * @since 2021-01-21
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/gen/template")
public class GenTemplateController extends BaseController {

    private final GenTemplateService genTemplateService;
    private final GenTemplateGroupService genTemplateGroupService;

    @GetMapping("/list")
    public R list(GenTemplateDTO dto) {
        IPage<GenTemplate> list = genTemplateService.page(initPage(dto), buildWrapper(dto));
        return R.ok(list);
    }

    @GetMapping("/info/{id}")
    public R getInfo(@PathVariable Long id) {
        GenTemplate info = genTemplateService.getById(id);
        return R.ok(info);
    }

    @GetMapping("/group/list")
    public R groupList(GenTemplateGroupDTO dto) {
        List<GenTemplateGroup> list = genTemplateGroupService.list(buildWrapper(dto));
        return R.ok(list);
    }

    @PostMapping("/update")
    public R update(@RequestBody @Validated GenTemplateDTO dto) {
        GenTemplate datasource = AmosUtils.copy(dto, GenTemplate.class);
        genTemplateService.saveOrUpdate(datasource);
        return R.ok();
    }

    @PostMapping("/remove")
    public R remove(@RequestBody Set<Long> ids) {
        UpdateWrapper<GenTemplate> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(GenTemplate::getId), ids)
                .set(AmosUtils.toDbField(GenTemplate::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        genTemplateService.update(uw);
        return R.ok();
    }

    @PostMapping("/group/update")
    public R updateGroup(@RequestBody @Validated GenTemplateGroupDTO dto) {
        GenTemplateGroup group = AmosUtils.copy(dto, GenTemplateGroup.class);
        genTemplateGroupService.saveOrUpdate(group);
        return R.ok();
    }

    @PostMapping("/group/remove")
    public R removeGroup(@RequestBody Set<Long> ids) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq(AmosUtils.toDbField(GenTemplateGroup::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_DEFAULT);
        long count = genTemplateGroupService.count(qw);
        if (count == 1) {
            return R.error("删除失败,模板组至少保留一个");
        }
        UpdateWrapper<GenTemplateGroup> uw = new UpdateWrapper<>();
        uw.in(AmosUtils.toDbField(GenTemplateGroup::getId), ids)
                .set(AmosUtils.toDbField(GenTemplateGroup::getIsDeleted), SystemConstant.SYS_DELETE_FLAG_ALREADY);
        genTemplateGroupService.update(uw);
        return R.ok();
    }


    @GetMapping("/cache-list")
    public R cacheList(GenTemplateDTO dto) {
        List<GenTemplate> list = genTemplateService.list(buildWrapper(dto));
        return R.ok(list);
    }

}
