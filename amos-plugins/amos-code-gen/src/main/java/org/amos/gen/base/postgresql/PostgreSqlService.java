package org.amos.gen.base.postgresql;

import org.amos.gen.base.GeneratorConfig;
import org.amos.gen.base.SQLService;
import org.amos.gen.base.TableSelector;

/**
 * @author tanghc
 */
public class PostgreSqlService implements SQLService {
    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new PostgreSqlTableSelector(new PostgreSqlColumnSelector(generatorConfig), generatorConfig);
    }

}
