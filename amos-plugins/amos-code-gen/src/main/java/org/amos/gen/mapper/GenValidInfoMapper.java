/**
 * Copyright (c) 2020-2030 LiuBoTao [1211265557@qq.com]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amos.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.amos.gen.entity.GenValidInfo;

/**
 * 代码生成字段信息配置 Mapper 接口
 *
 * @author CodeGenerator
 * @since 2021-01-17
 */
public interface GenValidInfoMapper extends BaseMapper<GenValidInfo> {

}
