package org.amos.gen.base;

import lombok.Data;

/**
 * @desc: 代码生成前信息
 * @author: liubt
 * @date: 2022-10-11 10:58
 **/
@Data
public class GenPreInfo {

    private GeneratorConfig genConfig;

    private GenInfo genInfo;
}
