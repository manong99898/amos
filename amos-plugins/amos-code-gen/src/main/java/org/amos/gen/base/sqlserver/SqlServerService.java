package org.amos.gen.base.sqlserver;

import org.amos.gen.base.GeneratorConfig;
import org.amos.gen.base.SQLService;
import org.amos.gen.base.TableSelector;

public class SqlServerService implements SQLService {

    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new SqlServerTableSelector(new SqlServerColumnSelector(generatorConfig), generatorConfig);
    }

}
