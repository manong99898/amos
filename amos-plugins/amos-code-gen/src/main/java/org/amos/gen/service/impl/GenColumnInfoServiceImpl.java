package org.amos.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.amos.gen.entity.GenColumnInfo;
import org.amos.gen.mapper.GenColumnMapper;
import org.amos.gen.service.GenColumnInfoService;
import org.springframework.stereotype.Service;

/**
 * 代码生成字段信息配置 服务实现类
 *
 * @author CodeGenerator
 * @since 2020-12-18
 */
@Service
public class GenColumnInfoServiceImpl extends ServiceImpl<GenColumnMapper, GenColumnInfo> implements GenColumnInfoService {

}
