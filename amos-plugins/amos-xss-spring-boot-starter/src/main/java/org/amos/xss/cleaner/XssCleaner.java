package org.amos.xss.cleaner;

/**
 * @desc: Xss 风险的数据清理接口
 * @author: liubt
 * @date: 2020-12-15 10:13
 **/
public interface XssCleaner {

    /**
     * 清理有 Xss 风险的文本
     *
     * @param html 原 html
     * @return 清理后的 html
     */
    String clean(String html);

}
