<h1 style="text-align: center">AMOS 快速开发脚手架</h1>
<div style="text-align: center">

[![AUR](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)](https://gitee.com/lbt-group/amos/blob/master/LICENSE)
[![star](https://gitee.com/lbt-group/amos/badge/star.svg?theme=white)](https://gitee.com/lbt-group/amos)
</div>

#### 框架介绍
采用 Spring Boot 2.7.0 、 MybatisPlus、 Sa-Token、Vue3的前后端分离的快速开发脚手架。
amos的业务和功能都设计为可插拔的依赖，方便用户自由组装和卸载。
#### 体验地址
> [https://www.amos.vin](https://www.amos.vin)
#### 项目源码

|     | 后端源码                             | 前端源码                                |
|-----|----------------------------------|-------------------------------------|
 |  码云   | https://gitee.com/lbt-group/amos | https://gitee.com/lbt-group/amos-ui |

#### 主要特性
- 使用最新技术栈，社区资源丰富。
- 模块可插拔设计，自由组合和拓展
- 支持数据字典，可方便地对一些状态进行管理
- 支持接口限流，避免恶意请求导致服务层压力过大
- 支持接口级别的功能权限与数据权限，可自定义操作
- 自定义权限注解与匿名接口注解，可快速对接口拦截与放行
- 对一些常用地前端组件封装：表格数据请求、数据字典等
- 前后端统一异常拦截处理，统一输出异常，避免繁琐的判断

####  系统功能
- 用户管理：提供用户的相关配置
- 角色管理：对权限与菜单进行分配，可根据部门设置角色的数据权限
- 菜单管理：已实现菜单动态路由，后端可配置化，支持多级菜单
- 部门管理：可配置系统组织架构，树形表格展示
- 岗位管理：配置各个部门的职位
- 字典管理：可维护常用一些固定的数据，如：状态，性别等

#### 项目结构
项目采用按功能分模块的开发方式，结构如下

- `amos-api` 对外开放接口模块
- `amos-core` 系统的核心模块，各种工具类，公共配置存在该模块
- `amos-plugins` 系统插件模块，集成各类系统应用starter以及各类依赖，可以选择性引入使用。
- `amos-server` 系统业务模块


#### 特别鸣谢

- 感谢 [lolicode](https://gitee.com/lolicode/scui) 大佬提供的前端模板

#### 项目捐赠


#### 反馈交流
- QQ交流群：